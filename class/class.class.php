<?php
class Classes{

	public $db;
	
	public function __construct(){
		$this->db = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
		if(mysqli_connect_errno()){
			echo 'Error: Could not connect to Database';
			exit;
	}
}
	public function new_class($tidnum,$course,$subjectid){
		$sql = "SELECT * FROM tbl_class WHERE subject = '$subjectid' AND batchid='$course'";
		$result=mysqli_query($this->db, $sql);
		$user_data = mysqli_fetch_array($result);
		$count_row=$result->num_rows;
		
		if($count_row==0){
			$sql="INSERT INTO `tbl_class` (`batchid`,`teacher`,`subject`,`date_create`,`time_create`,`date_update`,`time_update`)
			VALUES ('$course','$tidnum','$subjectid',now(),now(),now(),now())";
			$result=mysqli_query($this->db,$sql) or 
			die(mysqli_connect_errno()."nd ma butang ang new class.");
			return $result;
		}
		else{
			return false;
		}
	}

	public function get_classes(){
		$sql="SELECT * FROM tbl_class ORDER BY subject ASC";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}else
		return null;
	}
	public function getbatch($batchid){
        $sql="SELECT * FROM tbl_batch WHERE batchid='$batchid'";
        $check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}
    }
	public function batchstudent($batchid){
        $sql="SELECT * FROM tbl_batchstudent WHERE batchid='$batchid'";
        $check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}
    }
	public function get_student($stud_id){
        $sql="SELECT * FROM tbl_students WHERE stud_id='$stud_id'";
        $check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}
    }
	public function get_teacher($teacherid){
        $sql="SELECT * FROM tbl_teachers WHERE tidnum='$teacherid'";
        $check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}
    }
	public function count_student($batchid){
        $sql="SELECT * FROM tbl_batchstudent WHERE batchid='$batchid'";
        $check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		
		return $count_row;
		
    }

    public function delete_class($id){
        $sql="DELETE FROM `tbl_class` WHERE `class_id`='$id'";
        $result=mysqli_query($this->db,$sql) or 
			die(mysqli_connect_errno()."nd ma delete ang  class.");
			return $result;
    }    

}
?>