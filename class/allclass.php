<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <span style="font-weight:bold">Classes</span>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=classes&act=add">Enroll Class</a>
    </h2>
</div>
<link rel="stylesheet" type="text/css" href="../admin/css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../admin/css/basictable.css" />
<script type="text/javascript" src="../admin/js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<div class="agile-tables">
					<div class="w3l-table-info">
					  <h3>Classes</h3>
					    <table id="table">
						<thead>
						  <tr>
							<th>Subject</th>
                            <th>Teacher</th>
                            <th>Course/Batch</th>
							<th># of Students</th>
							<th>Option</th>
						  </tr>
						</thead>
						<tbody>
<?php
    $class = new Classes();
    $access = $class->get_classes();
    foreach($access as $value){
    $batchid = $value['batchid'];
	$teacherid = $value['teacher'];
	?>
        <tr>
            <td><?php echo $value['subject'];?></td>            
            <td><?php 
				$teacher = $class->get_teacher($teacherid);
				foreach($teacher as $row){
					echo $row['fname'].' '.$row['lname'];
				}
			?></td>
            <td><?php
			$batch = $class->getbatch($batchid);
				foreach($batch as $row){
					echo $row['coursecode'].'/'.$row['year'];
				}
			?>
			</td>      
			<td><?php echo $class->count_student($batchid);?></td>
            <td><a href="index.php?mod=classes&act=view&id=<?php echo $batchid?>">View</a></td>                
        </tr>
    <?php
    }
?>
						</tbody>
					  </table>
					</div>
</div>