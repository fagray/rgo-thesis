				<div class="banner">
					<h2>
						<a href="index.php">Home</a>
						<i class="fa fa-angle-right"></i>
						<a href="index.php?mod=classes">Classes</a>
						<i class="fa fa-angle-right"></i>
						<span>Enroll Class</span>
					</h2>
				</div>

<div class="panel panel-widget forms-panel w3-last-form">
        <div class="forms">
            <div class="form-three widget-shadow">
                <div class=" panel-body-inputin">
                    <form class="form-horizontal" action="../library/process.class.php?action=new" method="post">
						<div class="form-group">
                            <label class="col-md-2 control-label">Teacher</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                    </span>
                                <select name="tidnum" required>
                                    <option selected disabled></option>
                                        <?php
                                        $teacher = new Teacher();
                                        $access = $teacher->get_teacher();
                                        foreach($access as $value){
                                        ?>
                                            <option value="<?php echo $value['tidnum'];?>">
                                            <?php echo $value['fname'].' '.$value['lname'];?></option>
                                        <?php
                                        }
                                        ?>
                                </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Course / Batch</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                    </span>
                                <select name="course" required>
                                    <option selected disabled></option>
                                        <?php
                                        $batch = new Batch();
                                        $access = $batch->get_batch();
                                        foreach($access as $value){
                                        ?>
                                            <option value="<?php echo $value['batchid'];?>">
                                            <?php 
                                            echo $value['coursecode'].' / '.$value['year']?></option>
                                        <?php
                                        }
                                        ?>
                                </select>
                                </div>

                            </div>
                        </div>						
                        <div class="form-group">
                            <label class="col-md-2 control-label">Subject / Exam</label>
                            <div class="col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon">
                                    </span>
                                <select name="subject" required>
                                    <option selected disabled></option>
                                        <?php
                                        $subject = new Subject();
                                        $access = $subject->get_subject();
                                        foreach($access as $value){
                                        ?>
                                            <option value="<?php echo $value['subject_id'];?>">
                                            <?php echo $value['subject_id'].' / '.$value['examid'];?></option>
                                        <?php
                                        }
                                        ?>
                                </select>
                                </div>
                            </div>
                        </div>                        
                         <button type="submit" class="btn btn-default w3ls-button">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>