<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=classes">Classes</a>
        <i class="fa fa-angle-right"></i>
        <span style="font-weight:bold">View Class</span>
    </h2>
</div>
<link rel="stylesheet" type="text/css" href="../admin/css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../admin/css/basictable.css" />
<script type="text/javascript" src="../admin/js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<div class="agile-tables">
					<div class="w3l-table-info">
					  <h3>Classes</h3>
					    <table id="table">
						<thead>
						  <tr>
							<th>ID#</th>
                            <th>Name</th>
                            <th># of Take(s)</th>
							<th>Option</th>
						  </tr>
						</thead>
						<tbody>
<?php
    $class = new Classes();
	$batchid=$_GET['id'];
    $access = $class->batchstudent($batchid);
    foreach($access as $value){
	$stud_id=$value['stud_id'];	
	?>
        <tr>
            <td><?php echo $stud_id;?></td>
            <td>
				<?php
						$student=$class->get_student($stud_id);
						foreach($student as $row){
							$id = $row['stu_id'];
							echo $row['fname'].' '.$row['lname'];
							
						}
				?>
			</td>
			<td><?php echo $value['numtake'];?></td>			
            <td><a href="index.php?mod=students&act=view&id=<?php echo $id?>">View</a></td>                
        </tr>
    <?php
    }
?>
						</tbody>
					  </table>
					</div>
</div>