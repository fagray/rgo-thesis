<?php
session_start();
include('config/config.php');
include('users/class.users.php');
include('course/class.course.php');
include('subject/class.subject.php');
include('student/class.student.php');
$user = new User();
$userid = $_SESSION['userid'];

if(!$user->get_session()){
	header("location: system/login.php");
}
if($_SESSION['position']==Admin){
	header("location: admin/index.php");
}else if($_SESSION['position']==Student){
	header("location: users/student/index.php");
}else if($_SESSION['position']==Teacher){
	header("location: users/teachers/index.php");
}else if($_SESSION['position']==Personel){
	header("location: users/personel/index.php");
}else{
	header("location: system/login.php");
}

if(isset($_GET['q'])){
	$user->user_logout();
	header("location: system/login.php");
}


$module = (isset($_GET['mod']) && $_GET['mod'] != '') ? $_GET['mod'] : '';
$action = (isset($_GET['act']) && $_GET['act'] != '') ? $_GET['act'] : '';
$process = (isset($_GET['pro']) && $_GET['pro'] != '') ? $_GET['pro'] : '';
$id = (isset($_GET['id']) && $_GET['id'] != '') ? $_GET['id'] : '';
?>