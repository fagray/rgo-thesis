<?php
class Teacher{
	public $db;
	
	public function __construct(){
		$this->db = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
		if(mysqli_connect_errno()){
			echo "Error: Could connect to Database.";
			exit;
		}
	}

	public function check_member($keyword,$cat){
		$sql="SELECT * FROM tbl_teacher WHERE $cat like'$keyword%'";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		
		if($count_row > 0){
			return true;
		}else{
			return false;
		}
	}
	public function get_teacher(){
		$sql = "SELECT * FROM tbl_teachers";
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
	}

	public function new_teacher($dob,$newname,$id,$fname,$lname,$gender,$age,$civil,$email,$contactnum,$address,$course,$yeargrad,$school){
		$sql="SELECT * FROM tbl_teachers WHERE  fname='$fname' AND
		lname='$lname'";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		
		if($count_row == 0){
			$sql="INSERT INTO `rgo_db`.`tbl_teachers` (`dob`,`picture`,`tidnum`, `fname`, `lname`, `gender`, `age`, `civil`, `email`,
					`contactnum`, `haddress`, `course`, `yeargrad`, `school`,
					`date_create`,`time_create`,`date_update`,`time_update`)
			VALUES ('$dob','$newname','$id','$fname','$lname','$gender','$age','$civil','$email','$contactnum','$address','$course','$yeargrad','$school',now(),now(),now(),now())";
			$result=mysqli_query($this->db,$sql) or 
			die(mysqli_connect_errno()."nd ma butang ang new teacher.");
			return $result;
		}else{
			return false;
		}
	}

	public function last_id(){
		$sql = "select * from tbl_teachers ORDER BY `tea_id` DESC LIMIT 1;";
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
	}	

    
	public function new_user($password,$id){
		$sql="SELECT * FROM tbl_users WHERE usr_idnum='$id'";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		
		if($count_row == 0){
			$sql="INSERT INTO `tbl_users` (`usr_position`,`usr_idnum`,`usr_password`, `usr_date_added`, `usr_time_added`, `usr_date_modified`, `usr_time_modified`) 
			VALUES ('Student','$id','$password',now(),now(),now(),now())";
			$result=mysqli_query($this->db,$sql) or 
			die(mysqli_connect_errno()."nd ma butang ang new user.");
			return $result;
		}else{
			return false;
		}
	}
		
	public function search_member($keyword,$cat){
		$sql="SELECT * FROM tbl_teacher WHERE $cat like'$keyword%'";
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
	}
	public function get_allmember(){
		$sql = "SELECT * FROM tbl_teacher ORDER BY officer DESC";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}else
		return null;
	}
			
	public function get_member(){
		$sql = "SELECT * FROM tbl_teacher where officer=0";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}else
		return null;
	}
	public function count_request(){
		$sql = "SELECT * FROM tbl_request";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		
		return $count_row;
		}
	public function get_request(){
		$sql = "SELECT * FROM tbl_request";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}else
		return null;
	}
	public function get_officer(){
		$sql = "SELECT * FROM tbl_teacher where officer='1'";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}else
		return null;
	}	
	public function view_teacher($id){
        $sql="SELECT * FROM tbl_teachers WHERE `tea_id`='$id'";
        $result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type; 
    }
	public function view_course($id){
        $sql="SELECT * FROM tbl_course WHERE `coursecode`='$id'";
        $result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type; 
    }	
	public function delete_teacher($id){
	$sql="DELETE FROM `tbl_teachers` WHERE `tidnum`='$id'";
	$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
	return $check;
	}
	public function delete_user($id){
	$sql="DELETE FROM `tbl_users` WHERE `usr_idnum`='$id'";
	$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
	return $check;
	}	
	public function deny_member($id){
	$sql="DELETE FROM `tbl_request` WHERE `tea_id`='$id'";
	$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
	return $result;
	}	
		
	public function edit_member($tea_id,$idnum,$fname,$lname,$year,$section,$gender){
	$sql="UPDATE `tbl_teacher` SET idnum='$idnum', section='$section', `year`='$year', `fname`='$fname', `lname` = '$lname', `gender` = '$gender' WHERE  `tea_id`='$tea_id'";
	$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
	return $result;
	}
}