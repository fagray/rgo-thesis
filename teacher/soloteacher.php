
<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=teachers">Teachers</a>
        <i class="fa fa-angle-right"></i>
        <b><span>View Teacher</span></b>
    </h2>
</div>
<?php
	$id = (isset($_GET['id']) && $_GET['id'] != '') ? $_GET['id'] : '';
	$list = new Teacher();
    $items = $list->view_teacher($id);
?>
<?php 
    foreach($items as $value){
?>
<script>
    function validateDelete(){
        var agree = confirm("Are you sure to delete <?php echo $value['fname'].' '.$value['lname'] ?>");
        if(agree)
            return(true);
        else
            return(false);
    }
    function validateEdit(){
        var agree = confirm("Are you sure to save your work?");
        if(agree)
            return(true);
        else
            return(false);
    }    
</script>
 <a href="index.php?mod=teachers&act=edit&id=<?php echo $id?>" class="btn btn-default w3ls-button">Edit Personal Information</a>
                    <a href="index.php?mod=teachers&act=edit&id=<?php echo $id?>" class="btn btn-default w3ls-button">Edit Security</a>
                    <a href="../library/process.teacher.php?action=del&id=<?php echo $value['stud_id']?>" class="btn btn-default w3ls-button" onclick="javascript:return(validateDelete());">Delete Teacher</a>
<div class="panel panel-widget forms-panel w3-last-form">
    <div class="forms">
        <div class="form-three widget-shadow">
            
            <div class=" panel-body-inputin">
                <form class="form-horizontal" action="index.php?mod=teachers&act=reg"  method="post" enctype="multipart/form-data" >
                    <div class="form-group">
                            <img src="../student/picture/<?php echo $value['picture'];?>" width="200px" height="200px" style="margin-left:40%; border-radius:20px;">
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="fname" value="<?php echo $value['fname'].' '.$value['lname'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Gender</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['gender'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Date Of Birth</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">

                                </span>
                                <input type="date" value="<?php echo $value['dob'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Civil Status</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['civil'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contact Number</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['contactnum'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Home Address</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['haddress'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Field</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['field'];?>" id="field-1" required="true" class="form-control" readonly>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">School Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['school'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Year Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['yeargrad'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>				
                </form>
                	<?php }
	?>
            </div>
        </div>
    </div>
</div>