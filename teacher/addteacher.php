<script>
    function validateDelete(){
        var agree = confirm("Are you sure to delete this record?");
        if(agree)
            return(true);
        else
            return(false);
    }
    function validateEdit(){
        var agree = confirm("Are you sure to save your work?");
        if(agree)
            return(true);
        else
            return(false);
    }

    function checkPasswordMatch() {
    var password = $("#pass1").val();
    var confirmPassword = $("#pass2").val();
	if(password != "" && confirmPassword != ""){
    if (password != confirmPassword){
        var d = document.getElementById("passwordcheck")
        d.className = "form-group has-error";
        var a = document.getElementById("passwordcheck1")
        a.className = "form-group has-error";
        document.getElementById("pass1").style.borderColor = "#E34234";
        document.getElementById("pass2").style.borderColor = "#E34234";
        $("#divCheckPasswordMatch").html("Passwords doesn't match!");

    }
    else{
        $("#divCheckPasswordMatch").html("Passwords match.");
        var d = document.getElementById("passwordcheck")
        d.className = "form-group has-success";
        var a = document.getElementById("passwordcheck1")
        a.className += "form-group has-success";        
        document.getElementById("pass1").style.borderColor = "GREEN";
        document.getElementById("pass2").style.borderColor = "GREEN";        
    }
	}else{
         $("divCheckPasswordMatch").html("");
        var d = document.getElementById("passwordcheck")
        d.className = "form-group has-error";
        var a = document.getElementById("passwordcheck1")
        a.className = "form-group has-error";
    }
}

$(document).ready(function myFunction() {
   $("#pass1").keyup(checkPasswordMatch);
   $("#pass2").keyup(checkPasswordMatch);
});
    

function wait(){
    alert("Please wait until the admin confirms your request. Thank you!")
}
    function validateSubmit(){
        var agree = confirm("Are you sure?");
        if(agree)
            return(true);
        else
            return(false);
    }        
</script>
<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=teachers">Teachers</a>
        <i class="fa fa-angle-right"></i>
        <span>Add Teacher</span>
    </h2>
</div>

<div class="panel panel-widget forms-panel w3-last-form">
    <div class="forms">
        <div class="form-three widget-shadow">
            <div class=" panel-body-inputin">
                <form class="form-horizontal" action="index.php?mod=teachers&act=reg" method="post" enctype="multipart/form-data" onsubmit="javascript:return(myFunction(wait(validateSubmit())));">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Picture</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                    <input type="file" name="fileToUpload" id="fileToUpload" class="form-control" required>
                            </div>
                        </div>
                    </div>
				<?php
                    $list = new Teacher();
                    $items = $list->last_id();
                    foreach($items as $value){
                    $lastID = $value['tea_id']+1;
                    $year= date("ym");
                    $invID = str_pad($lastID, 3, '0', STR_PAD_LEFT);
                    $newidnum = $year.$invID;	
				?>                    
                    <div class="form-group">
                        <label class="col-md-2 control-label">ID Number</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input name="idnum" type="text" value="<?php echo $newidnum;?>" class="form-control1" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                <?php 
					}
                ?>    
                    <div class="form-group">
                        <label class="col-md-2 control-label">Email Address</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope-o"></i>
                                </span>
                                <input name="email" type="email" class="form-control1" placeholder="Email Address">
                            </div>
                        </div>
                    </div>
                    <div class="form-group" id="passwordcheck">
                        <label class="col-md-2 control-label">Password</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-key"></i>
                                </span>
                                <input id="pass1"type="password" name="password" class="form-control1" id="exampleInputPassword1" placeholder="Password">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <p class="divCheckPasswordMatch"></p>
                        </div>                        
                    </div>
                    <div class="form-group" id="passwordcheck1">
                        <label class="col-md-2 control-label">Repeat Password</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-key"></i>
                                </span>
                                <input id="pass2" type="password" name="password2" class="form-control1" id="exampleInputPassword2" placeholder="Repeat Password">
                            </div>
                        </div>
                        <div class="col-sm-2">
                            <p class="divCheckPasswordMatch"></p>
                        </div>
                        <div class="col-sm-2">
                            <p id="divCheckPasswordMatch"></p>
                        </div>                                                
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">First Name</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="fname" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Last Name</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="lname" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Gender</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <select name="gender" required>
                                <option selected disabled></option>
                                <option>Male</option>
                                <option>Female</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Date Of Birth</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar-o"></i>
                                </span>
                                <input type="date" name="dob" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Civil Status</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">

                                </span>
                                <select name="civil" required>
                                <option selected disabled></option>
                                <option>Single</option>
                                <option>Married</option>
                                <option>Separated</option>
                                <option>Widowed</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contact Number</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="contactnum" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Home Address</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="address" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Course</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                            <select name="course" required>
                                <option selected disabled></option>
                                    <?php
                                    $course = new Course();
                                    $access = $course->get_course();
                                    foreach($access as $value){
                                    ?>
                                        <option value="<?php echo $value['coursecode'];?>">
                                        <?php echo $value['description'];?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">School Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="school" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Year Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="yeargrad" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>										
                    <button type="submit" class="btn btn-default w3ls-button">Submit</button> 
                </form>
            </div>
        </div>
    </div>
</div>