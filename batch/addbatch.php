<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=batch">Batches</a>
        <i class="fa fa-angle-right"></i>
        <span style="font-weight:bold;">Add Batch</span>
    </h2>
</div>


<div class="panel panel-widget forms-panel w3-last-form">
    <div class="forms">
        <div class="form-three widget-shadow">
            <div class=" panel-body-inputin">
                <form class="form-horizontal" action="../library/process.batch.php?action=new" method="post">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Batch</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="batchyear" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Course</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                            <select name="coursecode" required>
                                <option selected disabled></option>
                                    <?php
                                    $course = new Course();
                                    $access = $course->get_course();
                                    foreach($access as $value){
                                    ?>
                                        <option value="<?php echo $value['coursecode'];?>">
                                        <?php echo $value['description'];?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                    </div>									 
                     <button type="submit" class="btn btn-default w3ls-button">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>