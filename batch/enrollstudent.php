<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=students">Student</a>
        <i class="fa fa-angle-right"></i>
        <span style="font-weight:bold;">Enroll Student</span>
    </h2>
</div>


<div class="panel panel-widget forms-panel w3-last-form">
    <div class="forms">
        <div class="form-three widget-shadow">
            <div class=" panel-body-inputin">
                <form class="form-horizontal" action="../library/process.batch.php?action=enroll" method="post">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Batch</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                                <?php
                                $id =  $_GET['id'];?>
                                <input type="hidden" name="stud_id" value="<?php echo $id;?>" />
                            <select name="batchid" required>
                                <option selected disabled></option>            
                                <?php
                                    $course=$_GET['course'];
                                    $batch = new Batch();
                                    $access = $batch->getbatch($course);
                                    foreach($access as $value){
                                    ?>
                                        <option value="<?php echo $value['batchid'];?>">
                                        <?php echo $value['year'].'/'.$value['coursecode'];?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                        
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label"># of takes</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="number" name="numtakes" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>									 
                     <button type="submit" class="btn btn-default w3ls-button">Submit</button>
                </form>
            </div>
        </div>
    </div>
</div>