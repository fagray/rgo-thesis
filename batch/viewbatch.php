<link rel="stylesheet" type="text/css" href="../admin/css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../admin/css/basictable.css" />
<script type="text/javascript" src="../admin/js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Batches</span>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=batch&act=add">Add Batch</a>
    </h2>
</div>

<div class="agile-tables">
					<div class="w3l-table-info">
					  <h3>Batches</h3>
					    <table id="table">
						<thead>
						  <tr>
							<th>Batch</th>
							<th>Course</th>
							<th># of Students</th>
							<th>Option</th>
						  </tr>
						</thead>
						<tbody>
<?php
    $batch = new Batch();
    $access = $batch->get_batch();
    foreach($access as $value){
	$batchid = $value['batchid'];
    ?>
        <tr>
            <td><?php echo $value['year'];?></td>
            <td><?php echo $value['coursecode'];?></td>
			<td><?php
			echo $batch->count_student($batchid);
				
			?>
			</td>             
            <td><a href="#">Edit</a>|<a href="#">Delete</a></td>                
        </tr>
    <?php
    }
?>
						</tbody>
					  </table>
					</div>
</div>

