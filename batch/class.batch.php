<?php
class Batch{

	public $db;
	
	public function __construct(){
		$this->db = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
		if(mysqli_connect_errno()){
			echo 'Error: Could not connect to Database';
			exit;
	}
}
	public function new_batch($batchyear,$coursecode){
		$sql = "SELECT * FROM tbl_batch WHERE year='$batchyear' AND coursecode='$coursecode'";		
		$result=mysqli_query($this->db, $sql);
		$user_data = mysqli_fetch_array($result);
		$count_row=$result->num_rows;
		
		if($count_row==0){
			$sql="INSERT INTO `tbl_batch` (`year`,`coursecode`,`date_create`,`time_create`,`date_update`,`time_update`)
			VALUES ('$batchyear','$coursecode',now(),now(),now(),now())";
			$result=mysqli_query($this->db,$sql) or 
			die(mysqli_connect_errno()."nd ma butang ang new batch.");
			return $result;
		}
		else{
			return false;
		}
	}
    public function getbatch($coursecode){
        $sql="SELECT * FROM tbl_batch WHERE coursecode='$coursecode'";
        $check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}
    }
	public function count_student($batchid){
        $sql="SELECT * FROM tbl_batchstudent WHERE batchid='$batchid'";
        $check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		
		return $count_row;
		
    }
	public function get_course($coursecode){
        $sql="SELECT * FROM tbl_course WHERE coursecode='$coursecode'";
        $check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}
    }
    public function enroll($batch,$numtakes,$id){
		$sql = "SELECT * FROM tbl_batchstudent WHERE batchid='$batch' AND stud_id='$id'";		
		$result=mysqli_query($this->db, $sql);
		$user_data = mysqli_fetch_array($result);
		
        $count_row=$result->num_rows;
		if($count_row==0){
			$sql="INSERT INTO `tbl_batchstudent` (`batchid`,`numtake`,`stud_id`,`date_create`,`time_create`,`date_update`,`time_update`)
			VALUES ('$batch','$numtakes','$id',now(),now(),now(),now())";
			$result=mysqli_query($this->db,$sql) or 
			die(mysqli_connect_errno()."nd ma butang ang new batch.");
			if($result){
                $sql="UPDATE `tbl_students` SET `enrolled`=1,`takenum`='$numtakes' WHERE  `stud_id`='$id'";
                $result=mysqli_query($this->db, $sql);
                $user_data = mysqli_fetch_array($result);
                return $result;
            }else{
                return false;
            }
		}
		else{
			return false;
		}
	}    

	public function get_batch(){
		$sql="SELECT * FROM tbl_batch WHERE batch_status='1' ORDER BY year ASC ";
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			$type[]=$row;
		}
		return $type;
		}else
		return null;
	}
	public function user_logout(){
		$_SESSION['login']=false;
		session_destroy();
	}	
}
?>