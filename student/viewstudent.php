<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <b>
        <span>Students</span>
        </b>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=students&act=add">Add Student</a>
    </h2>
</div>
<link rel="stylesheet" type="text/css" href="../admin/css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../admin/css/basictable.css" />
<script type="text/javascript" src="../admin/js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<div class="agile-tables">
					<div class="w3l-table-info">
					  <h3>Students</h3>
					    <table id="table">
						<thead>
						  <tr>
							<th></th>
							<th>Name</th>
                            <th>Course</th>
                            <th>School</th>
                            <th># of Takes</th>
                            <th>Option</th>  
						  </tr>
						</thead>
						<tbody>
<?php
    $student = new Student();
    $access = $student->get_student();
    foreach($access as $value){
    ?>
        <tr>
            <td><img src="../student/picture/<?php echo $value['picture'];?>" width="100px" height="100px" style="border-radius:20px;"></td>            
            <td><?php echo $value['fname'].' '.$value['lname'];?></td>
            <td><?php echo $value['course'];?></td>
            <td><?php echo $value['school'];?></td>
            <td><?php echo $value['takenum'];?></td>
            <td><a href="index.php?mod=students&act=view&id=<?php echo $value['stu_id']?>">View</a></td>                
        </tr>
    <?php
    }
?>
						</tbody>
					  </table>
					</div>
</div>