				<div class="banner">
					<h2>
						<a href="index.php">Home</a>
						<i class="fa fa-angle-right"></i>
						<a href="index.php?mod=students">Students</a>
						<i class="fa fa-angle-right"></i>
						<a href="http://localhost/rgo5/admin/index.php?mod=students&act=add">Add Student</a>
						<i class="fa fa-angle-right"></i>
						<span>Login Information</span>                        
					</h2>
				</div>
<div class="panel panel-widget forms-panel w3-last-form">
    <div class="forms">
        <div class="form-three widget-shadow">
            <div class=" panel-body-inputin">
                <form class="form-horizontal" action="../library/process.student.php?action=new" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="fname" value="<?php echo $_POST['fname'];?>">
                    <input type="hidden" name="lname" value="<?php echo $_POST['lname'];?>">
                    <input type="hidden" name="gender" value="<?php echo $_POST['gender'];?>">
                    <input type="hidden" name="dob" value="<?php echo $_POST['dob'];?>">
                    <input type="hidden" name="contactnum" value="<?php echo $_POST['contactnum'];?>">
                    <input type="hidden" name="civil" value="<?php echo $_POST['civil'];?>">
                    <input type="hidden" name="address" value="<?php echo $_POST['address'];?>">
                    <input type="hidden" name="course" value="<?php echo $_POST['course'];?>">
                    <input type="hidden" name="yeargrad" value="<?php echo $_POST['yeargrad'];?>">
                    <input type="hidden" name="school" value="<?php echo $_POST['school'];?>">                 
                    <div class="form-group">
                        <label class="col-md-2 control-label">Picture</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                    <input type="file" name="fileToUpload" id="fileToUpload" class="form-control" required>
                            </div>
                        </div>
                    </div>
				<?php
                    $list = new Student();
                    $items = $list->last_id();
                    foreach($items as $value){
                    $lastID = $value['stu_id']+1;
                    $year= date("y");
                    $invID = str_pad($lastID, 5, '0', STR_PAD_LEFT);
                    $newidnum = $year.$invID;	
				?>                    
                    <div class="form-group">
                        <label class="col-md-2 control-label">ID Number</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                    <i class="fa fa-user"></i>
                                </span>
                                <input name="idnum" type="text" value="<?php echo $newidnum;?>" class="form-control1" placeholder="" readonly/>
                            </div>
                        </div>
                    </div>
                <?php 
					}
                ?>    
                    <div class="form-group">
                        <label class="col-md-2 control-label">Email Address</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                    <i class="fa fa-envelope-o"></i>
                                </span>
                                <input name="email" type="email" class="form-control1" placeholder="Email Address">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Password</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-key"></i>
                                </span>
                                <input type="password" name="password" class="form-control1" id="exampleInputPassword1" placeholder="Password">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Repeat Password</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-key"></i>
                                </span>
                                <input type="password" name="password2" class="form-control1" id="exampleInputPassword2" placeholder="Repeat Password">
                            </div>
                        </div>
                    </div>										
                    <button type="submit" class="btn btn-default w3ls-button">Submit</button> 
                </form>
            </div>
        </div>
    </div>
</div>