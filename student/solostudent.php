<script>
    function validateDelete(){
        var agree = confirm("Are you sure to delete this Student?");
        if(agree)
            return(true);
        else
            return(false);
    }
    function validateEdit(){
        var agree = confirm("Are you sure to save your work?");
        if(agree)
            return(true);
        else
            return(false);
    }    
</script>
<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=students">Students</a>
        <i class="fa fa-angle-right"></i>
        <b><span>View Student</span></b>
    </h2>
</div>
<?php
	$id = (isset($_GET['id']) && $_GET['id'] != '') ? $_GET['id'] : '';
	$list = new Student();
    $items = $list->view_student($id);
?>
<?php 
    foreach($items as $value){
?>
                    <?php
                        if($value['enrolled']<1){
                            ?>
                    <a href="index.php?mod=batch&act=enroll&id=<?php echo $value['stud_id'];?>&course=<?php echo $value['course'];?>" class="btn btn-default w3ls-button">Enroll Student</a>
                    <?php
                        }else{
                    ?>

                    <a href="index.php?mod=students&act=viewgrades&id=<?php echo $value['stud_id'];?>&course=<?php echo $value['course'];?>" class="btn btn-default w3ls-button" style="text-decoration:underline">View Grade</a>

                    <?php        
                        }        
                      ?>


                    <a href="index.php?mod=students&act=edit&id=<?php echo $id?>" class="btn btn-default w3ls-button">Edit Personal Information</a>
                    <a href="index.php?mod=students&act=edit&id=<?php echo $id?>" class="btn btn-default w3ls-button">Edit Security</a>
                    <a href="../library/process.student.php?action=del&id=<?php echo $value['stud_id']?>" class="btn btn-default w3ls-button" onclick="javascript:return(validateDelete());">Delete Student</a>
<div class="panel panel-widget forms-panel w3-last-form">
    <div class="forms">
        <div class="form-three widget-shadow">
            
            <div class=" panel-body-inputin">
                <form class="form-horizontal" action="index.php?mod=students&act=reg"  method="post" enctype="multipart/form-data" >
                    <div class="form-group">
                            <img src="../student/picture/<?php echo $value['picture'];?>" width="200px" height="200px" style="margin-left:40%; border-radius:20px;">
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Name</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="fname" value="<?php echo $value['fname'].' '.$value['lname'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Number of Takes</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="fname" value="<?php echo $value['takenum'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>                    
                    <div class="form-group">
                        <label class="col-md-2 control-label">Gender</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['gender'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Date Of Birth</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">

                                </span>
                                <input type="date" value="<?php echo $value['dob'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Civil Status</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['civil'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contact Number</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['contactnum'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Home Address</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['haddress'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Course</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['course'];?>" id="field-1" required="true" class="form-control" readonly>

                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">School Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['school'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Year Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['yeargrad'];?>" id="field-1" required="true" class="form-control" readonly>
                            </div>
                        </div>
                    </div>				
                </form>
                	<?php }
	?>
            </div>
        </div>
    </div>
</div>