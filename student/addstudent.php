				<div class="banner">
					<h2>
						<a href="index.php">Home</a>
						<i class="fa fa-angle-right"></i>
						<a href="index.php?mod=students">Students</a>
						<i class="fa fa-angle-right"></i>
						<b>
                        <span>Add Student</span>
                        </b>
					</h2>
				</div>

<div class="panel panel-widget forms-panel w3-last-form">
    <div class="forms">
        <div class="form-three widget-shadow">
            <div class=" panel-body-inputin">
                <form class="form-horizontal" action="index.php?mod=students&act=reg" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-md-2 control-label">First Name</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="fname" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Last Name</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="lname" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Gender</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <select name="gender" required>
                                <option selected disabled></option>
                                <option>Male</option>
                                <option>Female</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Date Of Birth</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar-o"></i>
                                </span>
                                <input type="date" name="dob" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Civil Status</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">

                                </span>
                                <select name="civil" required>
                                <option selected disabled></option>
                                <option>Single</option>
                                <option>Married</option>
                                <option>Separated</option>
                                <option>Widowed</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contact Number</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="contactnum" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Home Address</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="address" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Course</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                            <select name="course" required>
                                <option selected disabled></option>
                                    <?php
                                    $course = new Course();
                                    $access = $course->get_course();
                                    foreach($access as $value){
                                    ?>
                                        <option value="<?php echo $value['coursecode'];?>">
                                        <?php echo $value['description'];?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">School Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <select name="school" required>
                                
                                <?php
                                        $school = new School();
                                        $access = $school->get_school();
                                        foreach($access as $value){
                                        ?>
                                            <option value="<?php echo $value['schoolid'];?>">
                                            <?php echo $value['description'];?></option>
                                        <?php
                                        }
                                ?>
                                    </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Year Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="yeargrad" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>										
                    <button type="submit" class="btn btn-default w3ls-button">Next</button> 
                </form>
            </div>
        </div>
    </div>
</div>