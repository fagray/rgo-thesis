<script>
    function validateDelete(){
        var agree = confirm("Are you sure to delete this Student?");
        if(agree)
            return(true);
        else
            return(false);
    }
    function validateEdit(){
        var agree = confirm("Are you sure to save your changes?");
        if(agree)
            return(true);
        else
            return(false);
    }    
</script>
<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=students">Students</a>
        <i class="fa fa-angle-right"></i>
        <b>
        <span>Edit Student Information</span>
        </b>
    </h2>
</div>
<?php
	$id = (isset($_GET['id']) && $_GET['id'] != '') ? $_GET['id'] : '';
	$list = new Student();
    $items = $list->view_student($id);
?> 
<?php 
    foreach($items as $value){
?>
<div class="panel panel-widget forms-panel w3-last-form">
    <div class="forms">
        <div class="form-three widget-shadow">
            
            <div class=" panel-body-inputin">
                <form class="form-horizontal" action="index.php?mod=students&act=edit" onsubmit="javascript:return(validateEdit())"method="post" enctype="multipart/form-data">
                    <div class="form-group">
                            <img src="../student/picture/<?php echo $value['picture'];?>" width="200px" height="200px" style="margin-left:40%; border-radius:20px;">
                    </div>
                   <div class="form-group">
                        <label class="col-md-2 control-label">First Name</label>
                        <div class="col-md-8">
                            <div class="input-group">							
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['fname']?>" name="fname" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Last Name</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['lname']?>" name="lname" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Gender</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <select name="gender" required>
                                <option selected disabled><?php echo $value['gender']?></option>
                                <option>Male</option>
                                <option>Female</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Date Of Birth</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar-o"></i>
                                </span>
                                <input type="date" value="<?php echo $value['dob']?>" name="dob" id="field-1" required="true" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Civil Status</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">

                                </span>
                                <select name="civil" required>
                                <option selected disabled><?php echo $value['civil']?></option>
                                <option>Single</option>
                                <option>Married</option>
                                <option>Separated</option>
                                <option>Widowed</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Contact Number</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['contactnum']?>" name="contactnum" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Home Address</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" value="<?php echo $value['haddress']?>" name="address" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">School Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="school" value="<?php echo $value['school']?>" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="col-md-2 control-label">Year Graduated</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                                <input type="text" name="yeargrad" value="<?php echo $value['yeargrad']?>" id="field-1" required="" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Course</label>
                        <div class="col-md-8">
                            <div class="input-group">
                                <span class="input-group-addon">
                                </span>
                            <select name="course" required>
                                <?php
                                    $course = new Student();
                                    $access = $course->view_course($value['course']);
                                    foreach($access as $value){
                                    ?>
                                    <option selected disabled value="<?php echo $value['coursecode'];?>">
                                        <?php echo $value['description'];?>
                                    </option>
                                    <?php
                                    }
                              
                                    $course = new Course();
                                    $access = $course->get_course();
                                    foreach($access as $value){
                                    ?>
                                        <option value="<?php echo $value['coursecode'];?>">
                                        <?php echo $value['description'];?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                            </div>
                        </div>
                    </div>
                    
                   
                    <a href="index.php?mod=students&act=view&id=<?php echo $value['stu_id']?>" class="btn btn-default w3ls-button"><< Back</a>
                   <center>
                     <button type="submit" class="btn btn-default w3ls-button">Save</button> 
                    </center>

                </form>
                	<?php }
	?>
            </div>
        </div>
    </div>
</div>