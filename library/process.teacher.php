<?php
include '../config/config.php';
include '../teacher/class.teacher.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch($action){
	case 'new':
			newTeacher();
			break;
	case 'edit':
			editStudent();
			break;
	case 'delete':
			deleteStudent();
			break;
		default:
			header("location: ../index.php");
}


function deleteMember(){
	$id = $_GET['id'];
	$member = new Member();
	$result = $member->delete_member($id);
	if(!$result){
		header("location: ../index.php?mod=member&act=member");
	}else{
		header("location: ../index.php?");
	}
}

function newTeacher(){
	$idnum = $_POST['teacher_id_num'];
	$password = $_POST['teacher_password'];
	$fname = $_POST['teacher_first_name'];
	$lname = $_POST['teacher_last_name'];
	$gender = $_POST['teacher_gender'];
	$age = $_POST['teacher_age'];
	$dob = $_POST['teacher_birthday'];
	$civil = $_POST['teacher_civil_status'];
	$email = $_POST['teacher_email'];
	$contactnum = $_POST['teacher_contact'];
	$haddress = $_POST['teacher_address'];
	$year = $_POST['teacher_graduated'];
	$school = $_POST['teacher_school'];
	$field = $_POST['teacher_field'];
	
	$teacher = new Teacher();
	$result = $teacher->new_teacher($dob,$idnum,$fname,$lname,$gender,$age,$civil,$email,$contactnum,$haddress,$field,$year,$school);
		if($result){
			$result1 = $teacher->new_user(md5($password),$idnum);
			header("location: ../index.php?mod=member&act=member");
		}else{
			header("location: ../index.php?");
		}
	
}
function denyMember(){
	$id = $_GET['id'];
	$member = new Member();
	$result = $member->deny_member($id);
	if(!$result){
			header("location: ../index.php?mod=member&act=request");
	}else{
		header("location: ../index.php?");
	}
}
function acceptMember(){
	$idnum = $_GET['idnum'];
	$password = $_GET['password'];
	$position = $_GET['position'];
	$fname = $_GET['fname'];
	$lname = $_GET['lname'];
	$year = $_GET['year'];
	$section = $_GET['section'];
	$gender = $_GET['gender'];
	if($position=='Member'){
		$officer='0';
	}else{
		$officer='1';
	}		
	$id = $_GET['id'];
	$member = new Member();
	$result23 = $member->deny_member($id);	
	$result1 = $member->new_user($password,$idnum);
	$result = $member->new_member($officer,$position,$idnum,$fname,$lname,$year,$section,$gender);
		if($result){
			header("location: ../index.php?mod=member&act=request");
		}else{
			header("location: ../index.php?");
		}
	
}
function request(){
	$idnum = $_POST['idnum'];
	$password = $_POST['password'];
	$position = 'Member';
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$year = $_POST['year'];
	$section = $_POST['section'];
	$gender = $_POST['gender'];
	$officer='0';
	$member = new Member();
	$result = $member->signup(md5($password),$officer,$position,$idnum,$fname,$lname,$year,$section,$gender);
		if($result){
			header("location: ../index.php?mod=member&act=member");
		}else{
			header("location: ../index.php?");
		}
	
}