<?php
include '../config/config.php';
include '../class/class.class.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch($action){
	case 'new':
			newClass();
			break;
	case 'edit':
			editClass();
			break;
	case 'delete':
			deletClass();
			break;
		default:
			header("location: ../index.php?mod=classs");
}

function newClass(){
	$tidnum = $_POST['tidnum'];
	$course = $_POST['course'];
    $subjectid = $_POST['subject'];

	$class = new Classes();
	$result = $class->new_class($tidnum,$course,$subjectid);
		if($result){
			header("location: ../admin/index.php?mod=classes");
		}else{
			header("location: ../index.php?error.php");
		}
}