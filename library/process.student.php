<?php
include '../config/config.php';
include '../student/class.student.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch($action){
	case 'new':
			newStudent();
			upload();
			break;
	case 'edit':
			editStudent();
			break;
	case 'del':
			deleteStudent();
			break;
		default:
			header("location: ../index.php");
}
function upload(){
	$target_dir = "../student/picture/";
	$ext = substr(strrchr($_FILES["fileToUpload"]["name"], "."), 1);
	$newname = $_POST['idnum'].'.'.basename($ext);
	$target_file = $target_dir . $newname;
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	$ext = substr(strrchr($_FILES["fileToUpload"]["name"], "."), 1);
	$ext = strtolower($ext);
	$newname = $_POST['idnum'].'.'.$ext;

	move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_file);

}

function deleteStudent(){
	$id = $_GET['id'];
	$student = new Student();
	$result = $student->delete_student($id);
	if($result){
		$user= $student->delete_user($id);
		header("location: ../admin/index.php?mod=students");
	}else{
		"<script> alert('SALA')</script>";
	}
}

function newStudent(){
	
	$idnum = $_POST['idnum'];
	$password = $_POST['password'];
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$gender = $_POST['gender'];
	$dob = $_POST['dob'];
	$civil = $_POST['civil'];
	$address = $_POST['address'];
	$course = $_POST['course'];
	$year = $_POST['yeargrad'];
	$school = $_POST['school'];
	$email = $_POST['email'];
	$contactnum = $_POST['contactnum'];
	
	$ext = substr(strrchr($_FILES["fileToUpload"]["name"], "."), 1);
	$ext = strtolower($ext);
	$newname = $_POST['idnum'].'.'.$ext;
	
	$student = new Student();
	$result = $student->new_student($dob,md5($password),$newname,$idnum,$fname,$lname,$gender,$civil,$email,$contactnum,$address,$course,$year,$school);
		if($result){
			$result1 = $student->new_user(md5($password),$idnum);
			header("location: ../admin/index.php?mod=students");
		}else{
			header("location: ../index.php?");
		}
	
}
function denyMember(){
	$id = $_GET['id'];
	$member = new Member();
	$result = $member->deny_member($id);
	if(!$result){
			header("location: ../index.php?mod=member&act=request");
	}else{
		header("location: ../index.php?");
	}
}
function acceptMember(){
	$idnum = $_GET['idnum'];
	$password = $_GET['password'];
	$position = $_GET['position'];
	$fname = $_GET['fname'];
	$lname = $_GET['lname'];
	$year = $_GET['year'];
	$section = $_GET['section'];
	$gender = $_GET['gender'];
	if($position=='Member'){
		$officer='0';
	}else{
		$officer='1';
	}		
	$id = $_GET['id'];
	$member = new Member();
	$result23 = $member->deny_member($id);	
	$result1 = $member->new_user($password,$idnum);
	$result = $member->new_member($officer,$position,$idnum,$fname,$lname,$year,$section,$gender);
		if($result){
			header("location: ../index.php?mod=member&act=request");
		}else{
			header("location: ../index.php?");
		}
	
}
function request(){
	$idnum = $_POST['idnum'];
	$password = $_POST['password'];
	$position = 'Member';
	$fname = $_POST['fname'];
	$lname = $_POST['lname'];
	$year = $_POST['year'];
	$section = $_POST['section'];
	$gender = $_POST['gender'];
	$officer='0';
	$member = new Member();
	$result = $member->signup(md5($password),$officer,$position,$idnum,$fname,$lname,$year,$section,$gender);
		if($result){
			header("location: ../index.php?mod=member&act=member");
		}else{
			header("location: ../index.php?");
		}
	
}