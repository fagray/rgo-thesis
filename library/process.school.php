<?php
include '../config/config.php';
include '../schools/class.school.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch($action){
	case 'new':
			newSchool();
			break;
	case 'edit':
			editSchool();
			break;
	case 'delete':
			deletSchool();
			break;
		default:
			header("location: ../index.php?mod=subjects");
}

function newSchool(){
	$schoolid = $_POST['schoolid'];
	$description = $_POST['description'];

	$school = new School();
	$result = $school->new_school($schoolid,$description);
		if($result){
			header("location: ../admin/index.php?mod=schools");
		}else{
			header("location: ../index.php?");
		}
	
}