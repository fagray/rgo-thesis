<?php
include '../config/config.php';
include '../batch/class.batch.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch($action){
	case 'enroll':
			enrollStudent();
			break;
    case 'new':
			newBatch();
			break;
	case 'edit':
			editBatch();
			break;
	case 'delete':
			deletBatch();
			break;
		default:
			header("location: ../index.php?mod=batch");
}

function enrollStudent(){
    $stud_id = $_POST['stud_id'];
	$numtakes = $_POST['numtakes'];
	$batchid = $_POST['batchid'];

	$batch = new Batch();
	$result = $batch->enroll($batchid,$numtakes,$stud_id);
		if($result){
			header("location: ../admin/index.php?mod=students");
		}else{
			header("location: ../index.php?");
		}
	
}

function newBatch(){
	$coursecode = $_POST['coursecode'];
	$batchyear = $_POST['batchyear'];

	$batch = new Batch();
	$result = $batch->new_batch($batchyear,$coursecode);
		if($result){
			header("location: ../admin/index.php?mod=batch");
		}else{
			header("location: ../index.php?");
		}
	
}