<?php
include '../config/config.php';
include '../exam/class.exam.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch($action){
	case 'new':
			newExam();
			break;
	case 'edit':
			editExam();
			break;
	case 'delete':
			deleteExam();
			break;
		default:
			header("location: ../index.php?mod=exams");
}

function newExam(){
	$examid = $_POST['examid'];
	$description = $_POST['description'];

	$exam = new Exam();
	$result = $exam->new_exam($examid,$description);
		if($result){
			header("location: ../admin/index.php?mod=exams");
		}else{
			header("location: ../index.php?");
		}
	
}
function deleteExam(){
	$examid = $_GET['examid'];
	$exam = new Exam();
	$result = $exam->delete_exam($examid);
		if($result){
			header("location: ../admin/index.php?mod=exams");
		}else{
			header("location: ../index.php?");
		}
	
}