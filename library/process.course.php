<?php
include '../config/config.php';
include '../course/class.course.php';

$action = (isset($_GET['action']) && $_GET['action'] != '') ? $_GET['action'] : '';

switch($action){
	case 'new':
			newCourse();
			break;
	case 'edit':
			editCourse();
			break;
	case 'delete':
			deletCourse();
			break;
		default:
			header("location: ../index.php?mod=subjects");
}

function newCourse(){
	$courseid = $_POST['courseid'];
	$description = $_POST['description'];

	$course = new Course();
	$result = $course->new_course($courseid,$description);
		if($result){
			header("location: ../admin/index.php?mod=courses");
		}else{
			header("location: ../index.php?");
		}
	
}