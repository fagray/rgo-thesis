-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.1.16-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win32
-- HeidiSQL Version:             9.3.0.4984
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping database structure for rgo_db
CREATE DATABASE IF NOT EXISTS `rgo_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `rgo_db`;


-- Dumping structure for table rgo_db.admin_tbl
CREATE TABLE IF NOT EXISTS `admin_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.admin_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `admin_tbl` DISABLE KEYS */;
INSERT INTO `admin_tbl` (`id`, `username`, `password`) VALUES
	(1, 'admin', '1234'),
	(2, 'leemark', 'leemark');
/*!40000 ALTER TABLE `admin_tbl` ENABLE KEYS */;


-- Dumping structure for table rgo_db.announcements_tbl
CREATE TABLE IF NOT EXISTS `announcements_tbl` (
  `anno_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `details` longtext,
  `priority` varchar(50) DEFAULT NULL,
  `date` varchar(50) NOT NULL,
  PRIMARY KEY (`anno_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table rgo_db.announcements_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `announcements_tbl` DISABLE KEYS */;
INSERT INTO `announcements_tbl` (`anno_id`, `title`, `details`, `priority`, `date`) VALUES
	(13, 'Wew', '					                Wew \r\nis an object-oriented programming language. When you do work in Java, you primarily use objects to get the job done. You create objects, modify them, change their variables, call their methods, and combine them with other objects. You develop classes, create objects out of those classes, and use them with other classes and objects.\r\n\r\nToday, you work extensively with objects as you undertake these essential tasks:\r\n\r\nCreating objects\r\nTesting and modifying their class and instance variables\r\nCalling an objectâ€™s methods\r\nConverting objects from one class to another\r\nCreating New Objects\r\nWhen you write a Java program, you define a set of classes. As you learned during Day 1, â€œGetting Started with Java,â€ a class is a template used to create one or more objects. These objects, which also are called instances, are self-contained elements of a program with related features and data. For the most part, you use the class merely to create instances and then work with those instances. In this section, you learn how to create a new object from any given class.\r\n                ', 'Teacher', '2016-09-07'),
	(14, 'What is java?', '																																																												           Java is an object-oriented programming language. When you do work in Java, you primarily use objects to get the job done. You create objects, modify them, change their variables, call their methods, and combine them with other objects. You develop classes, create objects out of those classes, and use them with other classes and objects.\r\n\r\nToday, you work extensively with objects as you undertake these essential tasks:\r\n\r\nCreating objects\r\nTesting and modifying their class and instance variables\r\nCalling an objectâ€™s methods\r\nConverting objects from one class to another\r\nCreating New Objects\r\nWhen you write a Java program, you define a set of classes. As you learned during Day 1, â€œGetting Started with Java,â€ a class is a template used to create one or more objects. These objects, which also are called instances, are self-contained elements of a program with related features and data. For the most part, you use the class merely to create instances and then work with those instances. In this section, you learn how to create a new object from any given class.\r\n                                                                                                                                                                                                     ', 'All', '2016-09-15');
/*!40000 ALTER TABLE `announcements_tbl` ENABLE KEYS */;


-- Dumping structure for table rgo_db.articles_tbl
CREATE TABLE IF NOT EXISTS `articles_tbl` (
  `arti_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) DEFAULT NULL,
  `details` longtext,
  `image` longtext,
  `priority` varchar(50) DEFAULT NULL,
  `date` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`arti_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.articles_tbl: ~2 rows (approximately)
/*!40000 ALTER TABLE `articles_tbl` DISABLE KEYS */;
INSERT INTO `articles_tbl` (`arti_id`, `title`, `details`, `image`, `priority`, `date`) VALUES
	(7, 'Bill Gates', '        Gates was born in Seattle, Washington on October 28, 1955. He is the son of William H. Gates, Sr.[b] and Mary Maxwell Gates. Gates\\\' ancestral origin includes English, German, and Irish, Scots-Irish.[18][19] His father was a prominent lawyer, and his mother served on the board of directors for First Interstate BancSystem and the United Way. Gates\\\'s maternal grandfather was JW Maxwell, a national bank president. Gates has one elder sister, Kristi (Kristianne), and one younger sister, Libby. He was the fourth of his name in his family, but was known as William Gates III or \\"Trey\\" because his father had the \\"II\\" suffix.[20] Early on in his life, Gates\\\'s parents had a law career in mind for him.[21] When Gates was young, his family regularly attended a church of the Congregational Christian Churches, a Protestant Reformed denomination.[22][23][24] The family encouraged competition; one visitor reported that \\"it didn\\\'t matter whether it was hearts or pickleball or swimming to the dock ... there was always a reward for winning and there was always a penalty for losing\\".[25]\r\n\r\nAt 13, he enrolled in the Lakeside School, a private preparatory school.[26] When he was in the eighth grade, the Mothers Club at the school used proceeds from Lakeside School\\\'s rummage sale to buy a Teletype Model 33 ASR terminal and a block of computer time on a General Electric (GE) computer for the school\\\'s students.[27] Gates took an interest in programming the GE system in BASIC, and was excused from math classes to pursue his interest. He wrote his first computer program on this machine: an implementation of tic-tac-toe that allowed users to play games against the computer. Gates was fascinated by the machine and how it would always execute software code perfectly. When he reflected back on that moment, he said, \\"There was just something neat about the machine.\\"[28] After the Mothers Club donation was exhausted, he and other students sought time on systems including DEC PDP minicomputers. One of these systems was a PDP-10 belonging to Computer Center Corporation (CCC), which banned four Lakeside students â€“ Gates, Paul Allen, Ric Weiland, and Kent Evans â€“ for the summer after it caught them exploiting bugs in the operating system to obtain free computer time.[29][30]\r\n\r\nAt the end of the ban, the four students offered to find bugs in CCC\\\'s software in exchange for computer time. Rather than use the system via Teletype, Gates went to CCC\\\'s offices and studied source code for various programs that ran on the system, including programs in Fortran, Lisp, and machine language. The arrangement with CCC continued until 1970, when the company went out of business. The following year, Information Sciences, Inc. hired the four Lakeside students to write a payroll program in Cobol, providing them computer time and royalties. After his administrators became aware of his programming abilities, Gates wrote the school\\\'s computer program to schedule students in classes. He modified the code so that he was placed in classes with \\"a disproportionate number of interesting girls.\\"[31] He later stated that \\"it was hard to tear myself away from a machine at which I could so unambiguously demonstrate success.\\"[28] At age 17, Gates formed a venture with Allen, called Traf-O-Data, to make traffic counters based on the Intel 8008 processor.[32] In early 1973, Bill Gates served as a congressional page in the U.S. House of Representatives.[33]\r\n\r\nGates graduated from Lakeside School in 1973, and was a National Merit Scholar.[34] He scored 1590 out of 1600 on the SAT[35] and enrolled at Harvard College in the autumn of 1973.[36] While at Harvard, he met Steve Ballmer, who would later succeed Gates as CEO of Microsoft.        ', 'photos/6855.jpg', 'All', '2016-08-30'),
	(9, 'RGO', '                Gates was born in Seattle, Washington on October 28, 1955. He is the son of William H. Gates, Sr.[b] and Mary Maxwell Gates. Gates\\\' ancestral origin includes English, German, and Irish, Scots-Irish.[18][19] His father was a prominent lawyer, and his mother served on the board of directors for First Interstate BancSystem and the United Way. Gates\\\'s maternal grandfather was JW Maxwell, a national bank president. Gates has one elder sister, Kristi (Kristianne), and one younger sister, Libby. He was the fourth of his name in his family, but was known as William Gates III or \\"Trey\\" because his father had the \\"II\\" suffix.[20] Early on in his life, Gates\\\'s parents had a law career in mind for him.[21] When Gates was young, his family regularly attended a church of the Congregational Christian Churches, a Protestant Reformed denomination.[22][23][24] The family encouraged competition; one visitor reported that \\"it didn\\\'t matter whether it was hearts or pickleball or swimming to the dock ... there was always a reward for winning and there was always a penalty for losing\\".[25]\r\nGates was born in Seattle, Washington on October 28, 1955. He is the son of William H. Gates, Sr.[b] and Mary Maxwell Gates. Gates\\\' ancestral origin includes English, German, and Irish, Scots-Irish.[18][19] His father was a prominent lawyer, and his mother served on the board of directors for First Interstate BancSystem and the United Way. Gates\\\'s maternal Gates was born in Seattle, Washington on October 28, 1955. He is the son of William H. Gates, Sr.[b] and Mary Maxwell Gates. Gates\\\' ancestral origin includes English, German, and Irish, Scots-Irish.[18][19] His father was a prominent lawyer, and his mother served on the board of directors for First Interstate BancSystem and the United Way. Gates\\\'s maternal grandfather was JW Maxwell, a national bank president. Gates has one elder sister, Kristi (Kristianne), and one younger sister, Libby. He was the fourth of his name in his family, but was known as William Gates III or \\"Trey\\" because his father had the \\"II\\" suffix.[20] Early on in his life, Gates\\\'s parents had a law career in mind for him.[21] When Gates was young, his family regularly attended a church of the Congregational Christian Churches, a Protestant Reformed denomination.[22][23][24] The family encouraged competition; one visitor reported that \\"it didn\\\'t matter whether it was hearts or pickleball or swimming to the dock ... there was always a reward for winning and there was always a penalty for losing\\".[25]\r\nGates was born in Seattle, Washington on October 28, 1955. He is the son of William H. Gates, Sr.[b] and Mary Maxwell Gates. Gates\\\' ancestral origin includes English, German, and Irish, Scots-Irish.[18][19] His father was a prominent lawyer, and his mother served on the board of directors for First Interstate BancSystem and the United Way. Gates\\\'s maternal Gates was born in Seattle, Washington on October 28, 1955. He is the son of William H. Gates, Sr.[b] and Mary Maxwell Gates. Gates\\\' ancestral origin includes English, German, and Irish, Scots-Irish.[18][19] His father was a prominent lawyer, and his mother served on the board of directors for First Interstate BancSystem and the United Way. Gates\\\'s maternal grandfather was JW Maxwell, a national bank president. Gates has one elder sister, Kristi (Kristianne), and one younger sister, Libby. He was the fourth of his name in his family, but was known as William Gates III or \\"Trey\\" because his father had the \\"II\\" suffix.[20] Early on in his life, Gates\\\'s parents had a law career in mind for him.[21] When Gates was young, his family regularly attended a church of the Congregational Christian Churches, a Protestant Reformed denomination.[22][23][24] The family encouraged competition; one visitor reported that \\"it didn\\\'t matter whether it was hearts or pickleball or swimming to the dock ... there was always a reward for winning and there was always a penalty for losing\\".[25]\r\nGates was born in Seattle, Washington on October 28, 1955. He is the son of William H. Gates, Sr.[b] and Mary Maxwell Gates. Gates\\\' ancestral origin includes English, German, and Irish, Scots-Irish.[18][19] His father was a prominent lawyer, and his mother served on the board of directors for First Interstate BancSystem and the United Way. Gates\\\'s maternal ', 'photos/wallpaper.jpg', 'All', '2016-08-31');
/*!40000 ALTER TABLE `articles_tbl` ENABLE KEYS */;


-- Dumping structure for table rgo_db.course_tbl
CREATE TABLE IF NOT EXISTS `course_tbl` (
  `course_id` int(11) NOT NULL AUTO_INCREMENT,
  `course` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.course_tbl: ~5 rows (approximately)
/*!40000 ALTER TABLE `course_tbl` DISABLE KEYS */;
INSERT INTO `course_tbl` (`course_id`, `course`) VALUES
	(1, 'BSN'),
	(2, 'RADTECH'),
	(3, 'CRIM'),
	(4, 'MED'),
	(5, 'PHD');
/*!40000 ALTER TABLE `course_tbl` ENABLE KEYS */;


-- Dumping structure for table rgo_db.questions_tbl
CREATE TABLE IF NOT EXISTS `questions_tbl` (
  `ques_id` int(11) NOT NULL AUTO_INCREMENT,
  `question_name` text NOT NULL,
  `answer1` varchar(250) NOT NULL,
  `answer2` varchar(250) NOT NULL,
  `answer3` varchar(250) NOT NULL,
  `answer4` varchar(250) NOT NULL,
  `answer` varchar(50) NOT NULL,
  `course_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ques_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.questions_tbl: ~11 rows (approximately)
/*!40000 ALTER TABLE `questions_tbl` DISABLE KEYS */;
INSERT INTO `questions_tbl` (`ques_id`, `question_name`, `answer1`, `answer2`, `answer3`, `answer4`, `answer`, `course_id`) VALUES
	(70, 'An organ of soft nervous tissue contained in the skull of vertebrates, functioning as the coordinating center of sensation and intellectual and nervous activity.', 'Liver', 'Brain', 'Heart', 'Kidney', '2', 1),
	(71, 'A framework of bone or cartilage enclosing the brain of a vertebrate; the skeleton of a person\'s or animal\'s head.', 'Skull', 'Liver', 'Tissue', 'Skin', '1', 1),
	(72, 'The thin layer of tissue forming the natural outer covering of the body of a person or animal.', 'Liver', 'Skull', 'Lungs', 'Skin', '4', 1),
	(73, 'The human skeleton is the internal framework of the body. It is composed of ____ bones at birth', '200', '273', '270', '206', '3', 1),
	(74, 'The emission of energy as electromagnetic waves or as moving subatomic particles, especially high-energy particles that cause ionization.', 'Radiation', 'Wifi', 'Bluetooth', 'Signal', '1', 2),
	(75, 'A gesture, action, or sound that is used to convey information or instructions, typically by prearrangement between the parties concerned.', 'Wifi', 'Bluetooth', 'Signal', 'Antena', '3', 2),
	(76, 'The scientific study of crime and criminals.', 'Information Technology', 'Criminology', 'Kagawad', 'Tanod', '2', 3),
	(77, 'A medicine or other substance which has a physiological effect when ingested or otherwise introduced into the body.', 'Medicine', 'Shabu', 'Candy', 'Drugs', '4', 3),
	(78, 'The science or practice of the diagnosis, treatment, and prevention of disease (in technical use often taken to exclude surgery).', 'Technology', 'Science', 'Medicine', 'Doctor', '4', 4),
	(79, 'An institution providing medical and surgical treatment and nursing care for sick or injured people.', 'Library', 'School', 'Plaza', 'Hospital', '4', 4),
	(80, 'Leemark is _______?', 'Hot', 'Hunk', 'Yummy', 'Handsome', '2', 2);
/*!40000 ALTER TABLE `questions_tbl` ENABLE KEYS */;


-- Dumping structure for table rgo_db.staff_tbl
CREATE TABLE IF NOT EXISTS `staff_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table rgo_db.staff_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `staff_tbl` DISABLE KEYS */;
INSERT INTO `staff_tbl` (`id`, `username`, `password`) VALUES
	(1, 'leemark', 'leemark');
/*!40000 ALTER TABLE `staff_tbl` ENABLE KEYS */;


-- Dumping structure for table rgo_db.student_tbl
CREATE TABLE IF NOT EXISTS `student_tbl` (
  `stud_id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  `score` int(11) DEFAULT NULL,
  `course_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stud_id`),
  KEY `course_id` (`course_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table rgo_db.student_tbl: ~9 rows (approximately)
/*!40000 ALTER TABLE `student_tbl` DISABLE KEYS */;
INSERT INTO `student_tbl` (`stud_id`, `username`, `password`, `score`, `course_id`) VALUES
	(1, 'leemark', 'leemark', NULL, 1),
	(2, 'mark2', 'mark2', NULL, 2),
	(3, 'mark3', 'mark3', NULL, 3),
	(4, 'mark4', 'mark4', NULL, 4),
	(5, 'Leemark_gwapo', 'Wew', NULL, 1),
	(6, 'Wew', 'Wew', NULL, 1),
	(7, 'Leemark_gwapo', 'Wew', NULL, 2),
	(8, 'Marco_polo', 'Wew', NULL, 4),
	(9, 'Ronaldo', 'Wew', NULL, 5);
/*!40000 ALTER TABLE `student_tbl` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_admin
CREATE TABLE IF NOT EXISTS `tbl_admin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  `First Name` varchar(50) NOT NULL DEFAULT '0',
  `Last Name` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_admin: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_admin` DISABLE KEYS */;
INSERT INTO `tbl_admin` (`id`, `username`, `password`, `First Name`, `Last Name`) VALUES
	(1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', 'Chris', 'Sorongon'),
	(2, 'leemark', 'leemark', '0', '0');
/*!40000 ALTER TABLE `tbl_admin` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_article
CREATE TABLE IF NOT EXISTS `tbl_article` (
  `art_id` int(50) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL DEFAULT '0',
  `body` varchar(20000) NOT NULL DEFAULT '0',
  `picture` varchar(100) NOT NULL DEFAULT '0',
  `date_create` date NOT NULL DEFAULT '0000-00-00',
  `time_create` time NOT NULL DEFAULT '00:00:00',
  `date_update` date NOT NULL DEFAULT '0000-00-00',
  `time_update` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`art_id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_article: ~2 rows (approximately)
/*!40000 ALTER TABLE `tbl_article` DISABLE KEYS */;
INSERT INTO `tbl_article` (`art_id`, `title`, `body`, `picture`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(12, 'asdasd', '', 'asdasd.jpg', '2016-07-06', '18:56:24', '2016-07-06', '18:56:24'),
	(13, 'Check', 'jerwin		', 'Check.jpg', '2016-07-06', '19:24:07', '2016-07-06', '19:24:07');
/*!40000 ALTER TABLE `tbl_article` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_balance
CREATE TABLE IF NOT EXISTS `tbl_balance` (
  `bal_id` int(11) NOT NULL AUTO_INCREMENT,
  `stud_id` varchar(50) NOT NULL DEFAULT '0',
  `balance` int(11) NOT NULL DEFAULT '0',
  `fee_id` int(11) NOT NULL,
  PRIMARY KEY (`bal_id`),
  KEY `fee_id` (`fee_id`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_balance: ~54 rows (approximately)
/*!40000 ALTER TABLE `tbl_balance` DISABLE KEYS */;
INSERT INTO `tbl_balance` (`bal_id`, `stud_id`, `balance`, `fee_id`) VALUES
	(1, '1600012', 0, 5),
	(2, '1600017', 0, 13),
	(3, '1600032', 0, 14),
	(4, '1600033', 0, 15),
	(5, '1600034', 0, 13),
	(6, '1600035', 0, 14),
	(7, '1600036', 0, 13),
	(8, '1600037', 0, 14),
	(9, '1600038', 0, 13),
	(10, '1600039', 0, 13),
	(11, '1600041', 0, 13),
	(12, '1600009', 0, 4),
	(13, '1600013', 15000, 4),
	(14, '1600015', 10000, 4),
	(15, '1600019', 15000, 4),
	(16, '1600020', 15000, 4),
	(17, '1600022', 12000, 6),
	(18, '1600027', 15000, 4),
	(19, '1600030', 15000, 4),
	(20, '1600031', 12000, 6),
	(21, '1600040', 6500, 7),
	(22, '1600014', 6000, 10),
	(23, '1600016', 6500, 10),
	(24, '1600018', 6500, 11),
	(25, '1600021', 6500, 12),
	(26, '1600023', 6500, 10),
	(27, '1600024', 6500, 10),
	(28, '1600025', 6500, 10),
	(29, '1600026', 6500, 12),
	(30, '1600028', 6500, 10),
	(31, '1600029', 3500, 11),
	(32, '1600042', 6500, 7),
	(33, '1600043', 0, 8),
	(34, '1600044', 6500, 9),
	(35, '1600045', 6500, 7),
	(36, '1600046', 6500, 7),
	(37, '1600047', 6500, 7),
	(38, 'TC16048', 6500, 7),
	(39, 'TC16049', 6500, 7),
	(40, 'TC16050', 6500, 7),
	(42, '1600017', 7500, 13),
	(43, '1600033', 7500, 14),
	(44, '1600032', 7500, 14),
	(45, '1600034', 7500, 13),
	(46, '1600035', 7500, 14),
	(47, '1600037', 7500, 14),
	(48, '1600036', 7500, 13),
	(49, '1600038', 7500, 13),
	(50, '1600039', 7500, 14),
	(51, '1600041', 7500, 14),
	(52, 'AG16051', 7500, 13),
	(53, 'AG16052', 7500, 13),
	(54, 'AG16053', 7500, 13),
	(55, 'AG16054', 7500, 13),
	(56, 'AG16055', 7500, 13),
	(57, 'AG16056', 7500, 15),
	(58, 'AG16057', 7500, 14),
	(59, '1600043', 6500, 8),
	(60, 'NU16058', 15000, 4),
	(61, 'NU16059', 15000, 4),
	(62, 'NU16060', 15000, 4),
	(63, 'CR16061', 6500, 10),
	(64, 'TC16062', 5500, 7),
	(65, 'CR16063', 6500, 12),
	(66, 'NU16064', 15000, 4);
/*!40000 ALTER TABLE `tbl_balance` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_batch
CREATE TABLE IF NOT EXISTS `tbl_batch` (
  `batchid` int(11) NOT NULL AUTO_INCREMENT,
  `batch` int(1) NOT NULL,
  `year` varchar(30) NOT NULL,
  `coursecode` varchar(30) NOT NULL,
  `start` date NOT NULL,
  `end` date NOT NULL,
  `date_create` date NOT NULL,
  `time_create` time NOT NULL,
  `date_update` date NOT NULL,
  `time_update` time NOT NULL,
  `batch_status` int(11) NOT NULL DEFAULT '1',
  `set1` int(11) NOT NULL DEFAULT '0',
  `set2` int(11) NOT NULL DEFAULT '0',
  `set3` int(11) NOT NULL DEFAULT '0',
  `set4` int(11) NOT NULL DEFAULT '0',
  `set5` int(11) NOT NULL DEFAULT '0',
  `finalexam` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`batchid`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_batch: 8 rows
/*!40000 ALTER TABLE `tbl_batch` DISABLE KEYS */;
INSERT INTO `tbl_batch` (`batchid`, `batch`, `year`, `coursecode`, `start`, `end`, `date_create`, `time_create`, `date_update`, `time_update`, `batch_status`, `set1`, `set2`, `set3`, `set4`, `set5`, `finalexam`) VALUES
	(7, 2, '2016', 'CRIM', '0000-00-00', '0000-00-00', '2016-08-06', '02:07:46', '2016-08-06', '02:07:46', 1, 1, 0, 0, 0, 0, 0),
	(8, 1, '2016', 'CRIM', '0000-00-00', '0000-00-00', '2016-08-06', '02:19:19', '2016-08-06', '02:19:19', 0, 1, 1, 1, 1, 1, 1),
	(9, 1, '2016', 'Nursing', '0000-00-00', '0000-00-00', '2016-08-06', '02:58:54', '2016-08-06', '02:58:54', 0, 1, 1, 1, 1, 1, 1),
	(10, 2, '2016', 'Nursing', '0000-00-00', '0000-00-00', '2016-08-06', '02:59:05', '2016-08-06', '02:59:05', 1, 1, 0, 0, 1, 1, 0),
	(12, 1, '2016', 'Agri', '0000-00-00', '0000-00-00', '2016-08-29', '18:04:49', '2016-08-29', '18:04:49', 0, 1, 1, 1, 1, 1, 1),
	(13, 2, '2016', 'Agri', '0000-00-00', '0000-00-00', '2016-08-29', '18:05:00', '2016-08-29', '18:05:00', 1, 1, 1, 0, 0, 0, 0),
	(14, 1, '2016', 'Teacher', '0000-00-00', '0000-00-00', '2016-08-29', '18:05:10', '2016-08-29', '18:05:10', 0, 1, 1, 1, 1, 1, 1),
	(15, 2, '2016', 'Teacher', '0000-00-00', '0000-00-00', '2016-08-29', '18:05:16', '2016-08-29', '18:05:16', 1, 1, 0, 0, 0, 0, 0);
/*!40000 ALTER TABLE `tbl_batch` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_batchstudent
CREATE TABLE IF NOT EXISTS `tbl_batchstudent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `batchid` varchar(30) NOT NULL,
  `stud_id` varchar(30) NOT NULL,
  `numtake` int(10) NOT NULL,
  `date_create` date NOT NULL,
  `time_create` time NOT NULL,
  `date_update` date NOT NULL,
  `time_update` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=96 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_batchstudent: 54 rows
/*!40000 ALTER TABLE `tbl_batchstudent` DISABLE KEYS */;
INSERT INTO `tbl_batchstudent` (`id`, `batchid`, `stud_id`, `numtake`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(71, '12', '1600039', 1, '2016-09-15', '17:05:40', '2016-09-15', '17:05:40'),
	(70, '12', '1600038', 0, '2016-09-15', '17:05:22', '2016-09-15', '17:05:22'),
	(69, '12', '1600036', 0, '2016-09-15', '17:05:10', '2016-09-15', '17:05:10'),
	(68, '12', '1600037', 2, '2016-09-15', '16:56:28', '2016-09-15', '16:56:28'),
	(67, '12', '1600035', 1, '2016-09-15', '16:46:54', '2016-09-15', '16:46:54'),
	(66, '12', '1600034', 0, '2016-09-15', '16:46:38', '2016-09-15', '16:46:38'),
	(65, '12', '1600032', 1, '2016-09-15', '16:46:22', '2016-09-15', '16:46:22'),
	(64, '12', '1600017', 0, '2016-09-15', '16:46:06', '2016-09-15', '16:46:06'),
	(63, '12', '1600033', 1, '2016-09-15', '16:45:27', '2016-09-15', '16:45:27'),
	(20, '9', '1600012', 1, '2016-09-07', '20:04:10', '2016-09-07', '20:04:10'),
	(31, '9', '1600009', 0, '2016-09-09', '01:25:02', '2016-09-09', '01:25:02'),
	(32, '9', '1600013', 0, '2016-09-09', '01:25:20', '2016-09-09', '01:25:20'),
	(33, '9', '1600015', 0, '2016-09-09', '01:25:36', '2016-09-09', '01:25:36'),
	(34, '9', '1600019', 0, '2016-09-09', '01:25:55', '2016-09-09', '01:25:55'),
	(35, '9', '1600020', 0, '2016-09-09', '01:27:16', '2016-09-09', '01:27:16'),
	(36, '9', '1600022', 1, '2016-09-09', '01:27:33', '2016-09-09', '01:27:33'),
	(37, '9', '1600027', 0, '2016-09-09', '01:27:48', '2016-09-09', '01:27:48'),
	(38, '9', '1600030', 0, '2016-09-09', '01:28:01', '2016-09-09', '01:28:01'),
	(39, '9', '1600031', 1, '2016-09-09', '01:28:25', '2016-09-09', '01:28:25'),
	(40, '14', '1600040', 0, '2016-09-09', '01:29:49', '2016-09-09', '01:29:49'),
	(41, '8', '1600014', 0, '2016-09-09', '01:30:33', '2016-09-09', '01:30:33'),
	(42, '8', '1600016', 0, '2016-09-09', '01:30:46', '2016-09-09', '01:30:46'),
	(43, '8', '1600018', 1, '2016-09-09', '01:31:01', '2016-09-09', '01:31:01'),
	(44, '8', '1600021', 1, '2016-09-09', '01:31:14', '2016-09-09', '01:31:14'),
	(45, '8', '1600023', 0, '2016-09-09', '01:31:26', '2016-09-09', '01:31:26'),
	(46, '8', '1600024', 0, '2016-09-09', '01:31:40', '2016-09-09', '01:31:40'),
	(47, '8', '1600025', 0, '2016-09-09', '01:31:54', '2016-09-09', '01:31:54'),
	(48, '8', '1600026', 1, '2016-09-09', '01:32:07', '2016-09-09', '01:32:07'),
	(49, '8', '1600028', 0, '2016-09-09', '01:32:19', '2016-09-09', '01:32:19'),
	(50, '8', '1600029', 1, '2016-09-09', '01:32:34', '2016-09-09', '01:32:34'),
	(51, '14', '1600042', 0, '2016-09-12', '01:29:04', '2016-09-12', '01:29:04'),
	(52, '14', '1600043', 1, '2016-09-12', '01:29:17', '2016-09-12', '01:29:17'),
	(53, '14', '1600044', 1, '2016-09-12', '01:29:33', '2016-09-12', '01:29:33'),
	(54, '14', '1600045', 0, '2016-09-12', '01:29:49', '2016-09-12', '01:29:49'),
	(55, '14', '1600046', 0, '2016-09-12', '01:30:03', '2016-09-12', '01:30:03'),
	(56, '14', '1600047', 0, '2016-09-12', '01:30:16', '2016-09-12', '01:30:16'),
	(57, '14', 'TC16048', 0, '2016-09-12', '01:30:28', '2016-09-12', '01:30:28'),
	(58, '14', 'TC16049', 0, '2016-09-12', '01:30:40', '2016-09-12', '01:30:40'),
	(59, '14', 'TC16050', 0, '2016-09-12', '01:30:51', '2016-09-12', '01:30:51'),
	(72, '12', '1600041', 1, '2016-09-15', '17:05:54', '2016-09-15', '17:05:54'),
	(76, '13', 'AG16051', 0, '2016-09-18', '01:34:35', '2016-09-18', '01:34:35'),
	(77, '13', '1600035', 1, '2016-09-18', '01:35:30', '2016-09-18', '01:35:30'),
	(78, '13', '1600036', 1, '2016-09-18', '01:35:54', '2016-09-18', '01:35:54'),
	(79, '13', '1600039', 2, '2016-09-18', '01:36:15', '2016-09-18', '01:36:15'),
	(80, '13', 'AG16052', 0, '2016-09-18', '02:16:18', '2016-09-18', '02:16:18'),
	(81, '13', 'AG16053', 0, '2016-09-18', '02:16:33', '2016-09-18', '02:16:33'),
	(82, '13', 'AG16054', 0, '2016-09-18', '02:16:46', '2016-09-18', '02:16:46'),
	(83, '13', 'AG16055', 0, '2016-09-18', '02:21:46', '2016-09-18', '02:21:46'),
	(84, '13', 'AG16056', 1, '2016-09-18', '02:22:02', '2016-09-18', '02:22:02'),
	(85, '13', 'AG16057', 1, '2016-09-18', '02:22:18', '2016-09-18', '02:22:18'),
	(86, '7', '1600028', 1, '2016-09-19', '01:45:50', '2016-09-19', '01:45:50'),
	(87, '15', '1600042', 1, '2016-09-19', '01:46:10', '2016-09-19', '01:46:10'),
	(88, '15', '1600043', 1, '2016-09-19', '01:46:27', '2016-09-19', '01:46:27'),
	(89, '10', 'NU16058', 0, '2016-09-19', '01:53:42', '2016-09-19', '01:53:42'),
	(90, '10', 'NU16059', 0, '2016-09-19', '10:37:52', '2016-09-19', '10:37:52'),
	(91, '10', 'NU16060', 0, '2016-09-19', '10:46:20', '2016-09-19', '10:46:20'),
	(92, '7', 'CR16061', 0, '2016-09-19', '10:59:07', '2016-09-19', '10:59:07'),
	(93, '15', 'TC16062', 0, '2016-09-19', '11:08:38', '2016-09-19', '11:08:38'),
	(94, '7', 'CR16063', 1, '2016-09-19', '11:29:08', '2016-09-19', '11:29:08'),
	(95, '10', 'NU16064', 0, '2016-09-19', '11:36:46', '2016-09-19', '11:36:46');
/*!40000 ALTER TABLE `tbl_batchstudent` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_class
CREATE TABLE IF NOT EXISTS `tbl_class` (
  `classid` int(11) NOT NULL AUTO_INCREMENT,
  `teacher` varchar(30) NOT NULL,
  `subjectid` varchar(50) NOT NULL,
  `submitgrade` int(1) NOT NULL DEFAULT '0',
  `batchid` int(5) NOT NULL,
  `class_status` int(5) NOT NULL DEFAULT '1',
  `date_create` date NOT NULL DEFAULT '0000-00-00',
  `time_create` time NOT NULL DEFAULT '00:00:00',
  `date_update` date NOT NULL DEFAULT '0000-00-00',
  `time_update` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=60 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_class: 23 rows
/*!40000 ALTER TABLE `tbl_class` DISABLE KEYS */;
INSERT INTO `tbl_class` (`classid`, `teacher`, `subjectid`, `submitgrade`, `batchid`, `class_status`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(10, '1609007', 'Set 1', 1, 9, 1, '2016-08-11', '02:41:46', '2016-08-11', '02:41:46'),
	(41, '1609007', 'Set 5', 1, 9, 1, '2016-09-15', '23:44:58', '2016-09-15', '23:44:58'),
	(9, '1608006', 'Set 1', 1, 8, 1, '2016-08-11', '01:30:56', '2016-08-11', '01:30:56'),
	(14, '1609008', 'Set 2', 1, 8, 1, '2016-09-10', '00:55:36', '2016-09-10', '00:55:36'),
	(15, '1609007', 'Set 2', 1, 9, 1, '2016-09-10', '00:56:26', '2016-09-10', '00:56:26'),
	(16, '1609009', 'Set 2', 1, 14, 1, '2016-09-10', '13:35:48', '2016-09-10', '13:35:48'),
	(47, '1609010', 'Set 4', 1, 12, 1, '2016-09-16', '01:35:27', '2016-09-16', '01:35:27'),
	(42, '1609007', 'Set 4', 1, 9, 1, '2016-09-15', '23:45:22', '2016-09-15', '23:45:22'),
	(19, '1609008', 'Set 3', 1, 8, 1, '2016-09-12', '01:37:43', '2016-09-12', '01:37:43'),
	(20, '1608006', 'Set 4', 1, 8, 1, '2016-09-12', '01:37:55', '2016-09-12', '01:37:55'),
	(21, '1608006', 'Set 5', 1, 8, 1, '2016-09-12', '01:38:15', '2016-09-12', '01:38:15'),
	(48, '1609010', 'Set 5', 1, 12, 1, '2016-09-16', '01:35:45', '2016-09-16', '01:35:45'),
	(46, '1609010', 'Set 3', 1, 12, 1, '2016-09-16', '01:35:14', '2016-09-16', '01:35:14'),
	(43, '1609007', 'Set 3', 1, 9, 1, '2016-09-15', '23:45:37', '2016-09-15', '23:45:37'),
	(44, '1609010', 'Set 1', 1, 12, 1, '2016-09-16', '00:57:55', '2016-09-16', '00:57:55'),
	(45, '1609010', 'Set 2', 1, 12, 1, '2016-09-16', '00:58:39', '2016-09-16', '00:58:39'),
	(49, '1609009', 'Set 1', 1, 14, 1, '2016-09-16', '01:46:05', '2016-09-16', '01:46:05'),
	(50, '1609009', 'Set 3', 1, 14, 1, '2016-09-16', '01:48:19', '2016-09-16', '01:48:19'),
	(51, '1609009', 'Set 4', 1, 14, 1, '2016-09-16', '01:49:33', '2016-09-16', '01:49:33'),
	(52, '1609009', 'Set 5', 1, 14, 1, '2016-09-16', '01:50:25', '2016-09-16', '01:50:25'),
	(54, '1609010', 'Set 1', 0, 13, 1, '2016-09-18', '01:50:08', '2016-09-18', '01:50:08'),
	(11, '1609008', 'Set 1', 1, 8, 1, '2016-08-15', '02:20:08', '2016-08-15', '02:20:08'),
	(55, '1609010', 'Set 2', 0, 13, 1, '2016-09-18', '02:22:40', '2016-09-18', '02:22:40'),
	(56, '1609007', 'Set 4', 0, 10, 1, '2016-09-19', '10:39:30', '2016-09-19', '10:39:30'),
	(57, '1608006', 'Set 1', 0, 7, 1, '2016-09-19', '10:59:30', '2016-09-19', '10:59:30'),
	(58, '1609009', 'Set 1', 0, 15, 1, '2016-09-19', '11:10:58', '2016-09-19', '11:10:58'),
	(59, '1609007', 'Set 5', 0, 10, 1, '2016-09-19', '11:37:45', '2016-09-19', '11:37:45');
/*!40000 ALTER TABLE `tbl_class` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_course
CREATE TABLE IF NOT EXISTS `tbl_course` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `coursecode` varchar(30) NOT NULL,
  `description` varchar(300) NOT NULL,
  `exam` varchar(3) NOT NULL,
  `date_create` date NOT NULL,
  `time_create` time NOT NULL,
  `date_update` date NOT NULL,
  `time_update` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_course: 4 rows
/*!40000 ALTER TABLE `tbl_course` DISABLE KEYS */;
INSERT INTO `tbl_course` (`id`, `coursecode`, `description`, `exam`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(6, 'Agri', 'Agriculturist', 'ALE', '2016-08-18', '00:29:27', '2016-08-18', '00:29:27'),
	(3, 'Nursing', 'Nursing', 'NLE', '2016-07-29', '21:15:52', '2016-07-29', '21:15:52'),
	(4, 'CRIM', 'Criminologist', 'CLE', '2016-08-06', '01:46:19', '2016-08-06', '01:46:19'),
	(5, 'Teacher', 'Teacher', 'LET', '2016-08-11', '02:17:12', '2016-08-11', '02:17:12');
/*!40000 ALTER TABLE `tbl_course` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_exam
CREATE TABLE IF NOT EXISTS `tbl_exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `examcode` varchar(30) NOT NULL,
  `description` varchar(300) NOT NULL,
  `date_create` date NOT NULL,
  `time_create` time NOT NULL,
  `date_update` date NOT NULL,
  `time_update` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_exam: 4 rows
/*!40000 ALTER TABLE `tbl_exam` DISABLE KEYS */;
INSERT INTO `tbl_exam` (`id`, `examcode`, `description`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(4, 'LET', 'Licensure Examination for Teachers', '2016-08-03', '21:45:44', '2016-08-03', '21:45:44'),
	(5, 'NLE', 'Nursing Licensure Exam', '2016-08-03', '21:46:39', '2016-08-03', '21:46:39'),
	(6, 'ALE', 'Agriculturist Licensure Examination', '2016-08-03', '21:48:22', '2016-08-03', '21:48:22'),
	(7, 'CLE', 'Criminologist Licensure Exam', '2016-08-03', '21:49:00', '2016-08-03', '21:49:00');
/*!40000 ALTER TABLE `tbl_exam` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_fees
CREATE TABLE IF NOT EXISTS `tbl_fees` (
  `fee_id` int(11) NOT NULL AUTO_INCREMENT,
  `coursecode` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(50) NOT NULL DEFAULT '0',
  `amount` int(10) NOT NULL DEFAULT '0',
  `date_create` date NOT NULL DEFAULT '0000-00-00',
  `time_create` time NOT NULL DEFAULT '00:00:00',
  `date_update` date NOT NULL DEFAULT '0000-00-00',
  `time_update` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`fee_id`),
  KEY `course_id` (`coursecode`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_fees: ~12 rows (approximately)
/*!40000 ALTER TABLE `tbl_fees` DISABLE KEYS */;
INSERT INTO `tbl_fees` (`fee_id`, `coursecode`, `description`, `amount`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(4, 'Nursing', 'First Taker', 15000, '2016-09-01', '02:44:09', '2016-09-01', '02:44:09'),
	(5, 'Nursing', 'Retaker(RGO)', 10000, '2016-09-01', '02:44:28', '2016-09-01', '02:44:28'),
	(6, 'Nursing', 'Retaker(Non-RGO)', 12000, '2016-09-01', '02:44:48', '2016-09-01', '02:44:48'),
	(7, 'Teacher', 'First Taker', 6500, '2016-09-01', '02:45:16', '2016-09-01', '02:45:16'),
	(8, 'Teacher', 'Retaker(RGO)', 6500, '2016-09-01', '02:45:27', '2016-09-01', '02:45:27'),
	(9, 'Teacher', 'Retaker(Non-RGO)', 6500, '2016-09-01', '02:45:40', '2016-09-01', '02:45:40'),
	(10, 'CRIM', 'First Taker', 6500, '2016-09-01', '02:46:04', '2016-09-01', '02:46:04'),
	(11, 'CRIM', 'Retaker(RGO)', 6500, '2016-09-01', '02:46:12', '2016-09-01', '02:46:12'),
	(12, 'CRIM', 'Retaker(Non-RGO)', 6500, '2016-09-01', '02:46:38', '2016-09-01', '02:46:38'),
	(13, 'Agri', 'First Taker', 7500, '2016-09-01', '02:46:56', '2016-09-01', '02:46:56'),
	(14, 'Agri', 'Retaker(RGO)', 7500, '2016-09-01', '02:47:06', '2016-09-01', '02:47:06'),
	(15, 'Agri', 'Retaker(Non-RGO)', 7500, '2016-09-01', '02:47:15', '2016-09-01', '02:47:15');
/*!40000 ALTER TABLE `tbl_fees` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_finalexam
CREATE TABLE IF NOT EXISTS `tbl_finalexam` (
  `fexam_id` int(11) NOT NULL AUTO_INCREMENT,
  `stud_id` varchar(10) NOT NULL,
  `year` varchar(50) NOT NULL,
  `batch` varchar(10) NOT NULL,
  `takenum` int(2) NOT NULL,
  `grade` int(2) NOT NULL,
  `result` varchar(10) DEFAULT NULL,
  `exam` varchar(10) NOT NULL,
  PRIMARY KEY (`fexam_id`),
  KEY `stud_id` (`stud_id`)
) ENGINE=InnoDB AUTO_INCREMENT=84 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_finalexam: ~45 rows (approximately)
/*!40000 ALTER TABLE `tbl_finalexam` DISABLE KEYS */;
INSERT INTO `tbl_finalexam` (`fexam_id`, `stud_id`, `year`, `batch`, `takenum`, `grade`, `result`, `exam`) VALUES
	(36, '1600030', '2015', '1st', 0, 75, 'Passed', 'NLE'),
	(37, '1600025', '2015', '1st', 1, 90, 'Passed', 'LET'),
	(38, '1600024', '2015', '1st', 0, 80, 'Passed', 'LET'),
	(39, '1600022', '2015', '1st', 0, 80, 'Passed', 'ALE'),
	(41, '1600021', '2015', '1st', 0, 75, 'Passed', 'CLE'),
	(42, '1600014', '2016', '1st', 0, 77, 'Passed', 'CLE'),
	(43, '1600016', '2016', '1st', 0, 78, 'Passed', 'CLE'),
	(44, '1600018', '2016', '1st', 1, 78, 'Passed', 'CLE'),
	(45, '1600021', '2016', '1st', 1, 78, 'Passed', 'CLE'),
	(46, '1600023', '2016', '1st', 0, 79, 'Passed', 'CLE'),
	(47, '1600024', '2016', '1st', 0, 78, 'Passed', 'CLE'),
	(48, '1600025', '2016', '1st', 0, 87, 'Passed', 'CLE'),
	(49, '1600026', '2016', '1st', 1, 86, 'Passed', 'CLE'),
	(50, '1600028', '2016', '1st', 0, 74, 'Failed', 'CLE'),
	(51, '1600029', '2016', '1st', 1, 85, 'Passed', 'CLE'),
	(52, '1600012', '2016', '1st', 0, 76, 'Passed', 'NLE'),
	(53, '1600009', '2016', '1st', 0, 78, 'Passed', 'NLE'),
	(54, '1600013', '2016', '1st', 0, 78, 'Passed', 'NLE'),
	(55, '1600015', '2016', '1st', 0, 88, 'Passed', 'NLE'),
	(56, '1600019', '2016', '1st', 0, 88, 'Passed', 'NLE'),
	(57, '1600020', '2016', '1st', 0, 76, 'Passed', 'NLE'),
	(58, '1600022', '2016', '1st', 1, 78, 'Passed', 'NLE'),
	(59, '1600027', '2016', '1st', 0, 98, 'Passed', 'NLE'),
	(60, '1600030', '2016', '1st', 1, 76, 'Passed', 'NLE'),
	(61, '1600031', '2016', '1st', 1, 77, 'Passed', 'NLE'),
	(64, '1600040', '2016', '1st', 0, 77, 'Passed', 'LET'),
	(65, '1600042', '2016', '1st', 0, 74, 'Failed', 'LET'),
	(66, '1600043', '2016', '1st', 1, 74, 'Failed', 'LET'),
	(67, '1600044', '2016', '1st', 1, 80, 'Passed', 'LET'),
	(68, '1600045', '2016', '1st', 0, 80, 'Passed', 'LET'),
	(69, '1600046', '2016', '1st', 0, 80, 'Passed', 'LET'),
	(70, '1600047', '2016', '1st', 0, 80, 'Passed', 'LET'),
	(71, 'TC16048', '2016', '1st', 0, 80, 'Passed', 'LET'),
	(72, 'TC16049', '2016', '1st', 0, 80, 'Passed', 'LET'),
	(73, 'TC16050', '2016', '1st', 0, 84, 'Passed', 'LET'),
	(74, '1600039', '2016', '1st', 1, 74, 'Failed', 'ALE'),
	(75, '1600038', '2016', '1st', 0, 75, 'Passed', 'ALE'),
	(76, '1600036', '2016', '1st', 0, 74, 'Failed', 'ALE'),
	(77, '1600037', '2016', '1st', 2, 75, 'Passed', 'ALE'),
	(78, '1600035', '2016', '1st', 1, 74, 'Failed', 'ALE'),
	(79, '1600034', '2016', '1st', 0, 78, 'Passed', 'ALE'),
	(80, '1600032', '2016', '1st', 1, 78, 'Passed', 'ALE'),
	(81, '1600017', '2016', '1st', 0, 89, 'Passed', 'ALE'),
	(82, '1600033', '2016', '1st', 1, 89, 'Passed', 'ALE'),
	(83, '1600041', '2016', '1st', 1, 75, 'Passed', 'ALE');
/*!40000 ALTER TABLE `tbl_finalexam` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_grade
CREATE TABLE IF NOT EXISTS `tbl_grade` (
  `grade_id` int(11) NOT NULL AUTO_INCREMENT,
  `grade` int(5) NOT NULL,
  `classid` int(5) NOT NULL,
  `batchid` int(5) NOT NULL,
  `stud_id` varchar(30) NOT NULL,
  `subjectid` varchar(30) NOT NULL,
  `date_create` date NOT NULL,
  `time_create` time NOT NULL,
  `date_update` date NOT NULL,
  `time_update` time NOT NULL,
  PRIMARY KEY (`grade_id`)
) ENGINE=MyISAM AUTO_INCREMENT=412 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_grade: 210 rows
/*!40000 ALTER TABLE `tbl_grade` DISABLE KEYS */;
INSERT INTO `tbl_grade` (`grade_id`, `grade`, `classid`, `batchid`, `stud_id`, `subjectid`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(71, 68, 11, 8, '1600029', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(70, 46, 11, 8, '1600028', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(69, 75, 11, 8, '1600026', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(68, 49, 11, 8, '1600025', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(67, 54, 11, 8, '1600024', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(66, 57, 11, 8, '1600023', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(65, 60, 11, 8, '1600021', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(64, 50, 11, 8, '1600018', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(63, 50, 11, 8, '1600016', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(62, 80, 11, 8, '1600014', 'Set 1', '2016-09-09', '22:08:12', '2016-09-09', '22:08:12'),
	(72, 60, 14, 8, '1600014', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(73, 77, 14, 8, '1600016', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(74, 63, 14, 8, '1600018', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(75, 64, 14, 8, '1600021', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(76, 87, 14, 8, '1600023', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(77, 78, 14, 8, '1600024', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(78, 98, 14, 8, '1600025', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(79, 67, 14, 8, '1600026', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(80, 73, 14, 8, '1600028', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(81, 75, 14, 8, '1600029', 'Set 2', '2016-09-10', '01:01:02', '2016-09-10', '01:01:02'),
	(82, 70, 19, 8, '1600014', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(83, 60, 19, 8, '1600016', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(84, 60, 19, 8, '1600018', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(85, 70, 19, 8, '1600021', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(86, 65, 19, 8, '1600023', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(87, 76, 19, 8, '1600024', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(88, 55, 19, 8, '1600025', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(89, 35, 19, 8, '1600026', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(90, 56, 19, 8, '1600028', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(91, 45, 19, 8, '1600029', 'Set 3', '2016-09-12', '01:38:38', '2016-09-12', '01:38:38'),
	(92, 56, 20, 8, '1600014', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(93, 57, 20, 8, '1600016', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(94, 54, 20, 8, '1600018', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(95, 34, 20, 8, '1600021', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(96, 56, 20, 8, '1600023', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(97, 35, 20, 8, '1600024', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(98, 65, 20, 8, '1600025', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(99, 42, 20, 8, '1600026', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(100, 46, 20, 8, '1600028', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(101, 36, 20, 8, '1600029', 'Set 4', '2016-09-12', '01:39:03', '2016-09-12', '01:39:03'),
	(102, 80, 21, 8, '1600014', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(103, 87, 21, 8, '1600016', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(104, 65, 21, 8, '1600018', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(105, 54, 21, 8, '1600021', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(106, 34, 21, 8, '1600023', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(107, 56, 21, 8, '1600024', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(108, 34, 21, 8, '1600025', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(109, 45, 21, 8, '1600026', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(110, 43, 21, 8, '1600028', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(111, 60, 21, 8, '1600029', 'Set 5', '2016-09-12', '01:39:30', '2016-09-12', '01:39:30'),
	(112, 53, 10, 9, '1600012', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(113, 56, 10, 9, '1600009', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(114, 45, 10, 9, '1600013', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(115, 65, 10, 9, '1600015', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(116, 45, 10, 9, '1600019', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(117, 76, 10, 9, '1600020', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(118, 42, 10, 9, '1600022', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(119, 41, 10, 9, '1600027', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(120, 56, 10, 9, '1600030', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(121, 77, 10, 9, '1600031', 'Set 1', '2016-09-12', '01:56:25', '2016-09-12', '01:56:25'),
	(122, 76, 9, 8, '1600014', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(123, 76, 9, 8, '1600016', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(124, 57, 9, 8, '1600018', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(125, 57, 9, 8, '1600021', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(126, 68, 9, 8, '1600023', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(127, 57, 9, 8, '1600024', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(128, 94, 9, 8, '1600025', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(129, 75, 9, 8, '1600026', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(130, 48, 9, 8, '1600028', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(131, 57, 9, 8, '1600029', 'Set 1', '2016-09-12', '01:59:51', '2016-09-12', '01:59:51'),
	(203, 60, 43, 9, '1600012', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(204, 70, 43, 9, '1600009', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(205, 67, 43, 9, '1600013', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(206, 60, 43, 9, '1600015', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(207, 76, 43, 9, '1600019', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(208, 63, 43, 9, '1600020', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(209, 67, 43, 9, '1600022', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(210, 78, 43, 9, '1600027', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(161, 50, 16, 14, 'TC16050', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(160, 39, 16, 14, 'TC16049', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(159, 59, 16, 14, 'TC16048', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(158, 40, 16, 14, '1600047', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(157, 59, 16, 14, '1600046', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(156, 96, 16, 14, '1600045', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(155, 50, 16, 14, '1600044', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(154, 30, 16, 14, '1600043', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(153, 80, 16, 14, '1600042', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(152, 46, 16, 14, '1600040', 'Set 2', '2016-09-12', '02:08:26', '2016-09-12', '02:08:26'),
	(201, 70, 15, 9, '1600031', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(200, 70, 15, 9, '1600030', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(199, 70, 15, 9, '1600027', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(198, 70, 15, 9, '1600022', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(197, 70, 15, 9, '1600020', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(196, 70, 15, 9, '1600019', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(195, 70, 15, 9, '1600015', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(194, 70, 15, 9, '1600013', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(193, 70, 15, 9, '1600009', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(192, 70, 15, 9, '1600012', 'Set 2', '2016-09-12', '02:24:55', '2016-09-12', '02:24:55'),
	(211, 69, 43, 9, '1600030', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(212, 68, 43, 9, '1600031', 'Set 3', '2016-09-15', '23:46:30', '2016-09-15', '23:46:30'),
	(213, 76, 42, 9, '1600012', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(214, 77, 42, 9, '1600009', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(215, 61, 42, 9, '1600013', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(216, 66, 42, 9, '1600015', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(217, 68, 42, 9, '1600019', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(218, 58, 42, 9, '1600020', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(219, 59, 42, 9, '1600022', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(220, 64, 42, 9, '1600027', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(221, 53, 42, 9, '1600030', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(222, 52, 42, 9, '1600031', 'Set 4', '2016-09-15', '23:47:05', '2016-09-15', '23:47:05'),
	(223, 57, 41, 9, '1600012', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(224, 78, 41, 9, '1600009', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(225, 46, 41, 9, '1600013', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(226, 56, 41, 9, '1600015', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(227, 76, 41, 9, '1600019', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(228, 58, 41, 9, '1600020', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(229, 76, 41, 9, '1600022', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(230, 65, 41, 9, '1600027', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(231, 46, 41, 9, '1600030', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(232, 35, 41, 9, '1600031', 'Set 5', '2016-09-15', '23:47:29', '2016-09-15', '23:47:29'),
	(233, 76, 47, 12, '1600039', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(234, 78, 47, 12, '1600038', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(235, 76, 47, 12, '1600036', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(236, 87, 47, 12, '1600037', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(237, 87, 47, 12, '1600035', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(238, 67, 47, 12, '1600034', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(239, 78, 47, 12, '1600032', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(240, 73, 47, 12, '1600017', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(241, 76, 47, 12, '1600033', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(242, 59, 47, 12, '1600041', 'Set 4', '2016-09-16', '01:40:20', '2016-09-16', '01:40:20'),
	(243, 80, 48, 12, '1600039', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(244, 77, 48, 12, '1600038', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(245, 78, 48, 12, '1600036', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(246, 87, 48, 12, '1600037', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(247, 76, 48, 12, '1600035', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(248, 86, 48, 12, '1600034', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(249, 79, 48, 12, '1600032', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(250, 90, 48, 12, '1600017', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(251, 84, 48, 12, '1600033', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(252, 85, 48, 12, '1600041', 'Set 5', '2016-09-16', '01:41:46', '2016-09-16', '01:41:46'),
	(253, 60, 46, 12, '1600039', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(254, 75, 46, 12, '1600038', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(255, 86, 46, 12, '1600036', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(256, 75, 46, 12, '1600037', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(257, 75, 46, 12, '1600035', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(258, 76, 46, 12, '1600034', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(259, 87, 46, 12, '1600032', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(260, 87, 46, 12, '1600017', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(261, 88, 46, 12, '1600033', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(262, 79, 46, 12, '1600041', 'Set 3', '2016-09-16', '01:43:00', '2016-09-16', '01:43:00'),
	(263, 87, 44, 12, '1600039', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(264, 87, 44, 12, '1600038', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(265, 89, 44, 12, '1600036', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(266, 87, 44, 12, '1600037', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(267, 78, 44, 12, '1600035', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(268, 76, 44, 12, '1600034', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(269, 67, 44, 12, '1600032', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(270, 64, 44, 12, '1600017', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(271, 65, 44, 12, '1600033', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(272, 50, 44, 12, '1600041', 'Set 1', '2016-09-16', '01:43:31', '2016-09-16', '01:43:31'),
	(273, 80, 45, 12, '1600039', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(274, 80, 45, 12, '1600038', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(275, 80, 45, 12, '1600036', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(276, 80, 45, 12, '1600037', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(277, 80, 45, 12, '1600035', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(278, 80, 45, 12, '1600034', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(279, 80, 45, 12, '1600032', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(280, 85, 45, 12, '1600017', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(281, 75, 45, 12, '1600033', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(282, 75, 45, 12, '1600041', 'Set 2', '2016-09-16', '01:43:55', '2016-09-16', '01:43:55'),
	(283, 80, 49, 14, '1600040', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(284, 80, 49, 14, '1600042', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(285, 80, 49, 14, '1600043', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(286, 80, 49, 14, '1600044', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(287, 80, 49, 14, '1600045', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(288, 80, 49, 14, '1600046', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(289, 80, 49, 14, '1600047', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(290, 80, 49, 14, 'TC16048', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(291, 80, 49, 14, 'TC16049', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(292, 80, 49, 14, 'TC16050', 'Set 1', '2016-09-16', '01:46:18', '2016-09-16', '01:46:18'),
	(293, 80, 50, 14, '1600040', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(294, 80, 50, 14, '1600042', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(295, 78, 50, 14, '1600043', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(296, 78, 50, 14, '1600044', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(297, 78, 50, 14, '1600045', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(298, 78, 50, 14, '1600046', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(299, 74, 50, 14, '1600047', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(300, 74, 50, 14, 'TC16048', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(301, 74, 50, 14, 'TC16049', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(302, 66, 50, 14, 'TC16050', 'Set 3', '2016-09-16', '01:48:37', '2016-09-16', '01:48:37'),
	(303, 80, 51, 14, '1600040', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(304, 80, 51, 14, '1600042', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(305, 80, 51, 14, '1600043', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(306, 80, 51, 14, '1600044', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(307, 80, 51, 14, '1600045', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(308, 80, 51, 14, '1600046', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(309, 89, 51, 14, '1600047', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(310, 78, 51, 14, 'TC16048', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(311, 66, 51, 14, 'TC16049', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(312, 77, 51, 14, 'TC16050', 'Set 4', '2016-09-16', '01:49:45', '2016-09-16', '01:49:45'),
	(411, 50, 52, 14, 'TC16050', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(410, 50, 52, 14, 'TC16049', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(409, 50, 52, 14, 'TC16048', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(408, 50, 52, 14, '1600047', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(407, 50, 52, 14, '1600046', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(406, 50, 52, 14, '1600045', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(405, 50, 52, 14, '1600044', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(404, 50, 52, 14, '1600043', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(402, 50, 52, 14, '1600040', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13'),
	(403, 50, 52, 14, '1600042', 'Set 5', '2016-09-16', '19:19:13', '2016-09-16', '19:19:13');
/*!40000 ALTER TABLE `tbl_grade` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_payment
CREATE TABLE IF NOT EXISTS `tbl_payment` (
  `payment_id` int(50) NOT NULL AUTO_INCREMENT,
  `stud_id` varchar(30) NOT NULL,
  `amount` int(20) NOT NULL,
  `pidnum` varchar(50) NOT NULL,
  `date_received` date NOT NULL,
  `time_received` time NOT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `pidnum` (`pidnum`)
) ENGINE=MyISAM AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_payment: 27 rows
/*!40000 ALTER TABLE `tbl_payment` DISABLE KEYS */;
INSERT INTO `tbl_payment` (`payment_id`, `stud_id`, `amount`, `pidnum`, `date_received`, `time_received`) VALUES
	(2, '1600012', 500, 'admin', '2016-01-08', '21:28:40'),
	(3, '1600012', 1000, 'admin', '2016-01-08', '21:44:22'),
	(4, '1600012', 5000, 'admin', '2016-02-08', '21:47:30'),
	(5, '1600012', 3400, 'admin', '2016-02-08', '21:47:59'),
	(6, '1600014', 500, 'admin', '2016-03-10', '13:33:27'),
	(9, '1600033', 7500, 'admin', '2016-04-13', '01:59:40'),
	(8, '1600032', 7500, 'admin', '2016-04-13', '01:59:28'),
	(7, '1600017', 7500, 'admin', '2016-03-13', '01:58:31'),
	(10, '1600034', 7500, 'admin', '2016-05-13', '01:59:52'),
	(11, '1600035', 7500, 'admin', '2016-05-13', '02:00:04'),
	(12, '1600036', 7500, 'admin', '2016-06-13', '02:00:16'),
	(13, '1600037', 7500, 'admin', '2016-06-13', '02:00:28'),
	(14, '1600038', 7500, 'admin', '2016-07-13', '02:00:48'),
	(15, '1600039', 7500, 'admin', '2016-07-13', '02:01:01'),
	(16, '1600041', 7500, 'admin', '2016-08-13', '02:02:11'),
	(17, '1600009', 500, 'admin', '2016-08-14', '00:10:06'),
	(18, '1600009', 500, 'admin', '2016-09-14', '00:10:13'),
	(19, '1600009', 500, 'admin', '2016-09-14', '00:10:21'),
	(20, '1600009', 500, 'admin', '2016-09-14', '00:10:28'),
	(21, '1600009', 500, 'admin', '2016-09-14', '00:10:34'),
	(22, '1600009', 500, 'admin', '2016-09-14', '00:10:41'),
	(23, '1600009', 500, 'admin', '2016-09-14', '00:10:50'),
	(24, '1600009', 1000, 'admin', '2016-09-16', '21:30:18'),
	(25, '1600012', 100, 'admin', '2016-09-17', '23:17:59'),
	(26, '1600009', 10000, 'admin', '2016-09-17', '23:18:28'),
	(27, '1600009', 500, 'admin', '2016-09-17', '23:18:37'),
	(28, '1600043', 6500, 'admin', '2016-09-18', '11:42:12'),
	(29, '1600029', 3000, 'admin', '2016-09-19', '10:14:00'),
	(30, '1600015', 5000, 'admin', '2016-09-19', '10:33:33'),
	(31, 'TC16062', 1000, 'admin', '2016-09-19', '11:09:03');
/*!40000 ALTER TABLE `tbl_payment` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_personnels
CREATE TABLE IF NOT EXISTS `tbl_personnels` (
  `per_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `pidnum` varchar(50) NOT NULL,
  `usr_password` varchar(50) NOT NULL DEFAULT '',
  `picture` varchar(100) NOT NULL,
  `fname` varchar(180) NOT NULL DEFAULT '',
  `lname` varchar(180) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `civil` varchar(180) NOT NULL DEFAULT '',
  `email` varchar(180) NOT NULL DEFAULT '',
  `contactnum` varchar(50) NOT NULL DEFAULT '',
  `haddress` varchar(180) NOT NULL DEFAULT '',
  `date_create` date NOT NULL DEFAULT '0000-00-00',
  `time_create` time NOT NULL DEFAULT '00:00:00',
  `date_update` date NOT NULL DEFAULT '0000-00-00',
  `time_update` time NOT NULL DEFAULT '00:00:00',
  `status` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`per_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_personnels: 1 rows
/*!40000 ALTER TABLE `tbl_personnels` DISABLE KEYS */;
INSERT INTO `tbl_personnels` (`per_id`, `pidnum`, `usr_password`, `picture`, `fname`, `lname`, `gender`, `dob`, `civil`, `email`, `contactnum`, `haddress`, `date_create`, `time_create`, `date_update`, `time_update`, `status`) VALUES
	(8, 'Jackwander', '202cb962ac59075b964b07152d234b70', 'Jackwander.jpg', 'Jerwin', 'Arnado', 'Male', '1995-08-12', 'Single', 'jerwin.arnado@gmail.com', '09497917906', 'Prk. Dalawidaw, Brgy. 16, Bacolod City, Negros Island Region', '2016-08-22', '22:40:40', '2016-08-22', '22:40:40', 1);
/*!40000 ALTER TABLE `tbl_personnels` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_schedclass
CREATE TABLE IF NOT EXISTS `tbl_schedclass` (
  `schedid` int(11) NOT NULL AUTO_INCREMENT,
  `classid` int(5) NOT NULL,
  `tidnum` varchar(10) NOT NULL DEFAULT '',
  `subject` varchar(10) NOT NULL DEFAULT '',
  `batchid` int(5) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `fromtime` time NOT NULL DEFAULT '00:00:00',
  `totime` time NOT NULL DEFAULT '00:00:00',
  `note` varchar(300) NOT NULL,
  `date_create` date NOT NULL DEFAULT '0000-00-00',
  `time_create` time NOT NULL DEFAULT '00:00:00',
  `date_update` date NOT NULL DEFAULT '0000-00-00',
  `time_update` time NOT NULL DEFAULT '00:00:00',
  `move` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`schedid`),
  KEY `classid` (`classid`)
) ENGINE=MyISAM AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_schedclass: 3 rows
/*!40000 ALTER TABLE `tbl_schedclass` DISABLE KEYS */;
INSERT INTO `tbl_schedclass` (`schedid`, `classid`, `tidnum`, `subject`, `batchid`, `date`, `fromtime`, `totime`, `note`, `date_create`, `time_create`, `date_update`, `time_update`, `move`) VALUES
	(57, 14, '1609008', 'Set 2', 8, '2016-09-10', '17:30:00', '22:30:00', 'Resched ta guys ky grabe ulan d sa amon. change is coming daan.\r\nResched ta guys ky grabe ulan d sa amon. change is coming daan.\r\nResched ta guys ky grabe ulan d sa amon. change is coming daan.\r\nResched ta guys ky grabe ulan d sa amon. change is coming daan.', '2016-09-17', '00:47:16', '2016-09-19', '01:43:26', 1),
	(58, 47, '1609010', 'Set 4', 12, '2016-09-23', '14:30:00', '17:30:00', 'Dala foods please', '2016-09-18', '12:00:31', '2016-09-18', '12:00:31', 0),
	(59, 54, '1609010', 'Set 1', 13, '2016-09-26', '01:30:00', '15:30:00', 'Resched ky ga baha.', '2016-09-19', '01:37:31', '2016-09-19', '01:38:29', 1),
	(60, 55, '1609010', 'Set 2', 13, '2016-09-21', '12:30:00', '17:30:00', 'Guys ma klase ta gli, didto gyapun sa dating tagpuan.', '2016-09-19', '08:21:03', '2016-09-19', '08:21:03', 0),
	(61, 10, '1609007', 'Set 1', 9, '2016-09-20', '07:30:00', '10:30:00', 'Please tell your classmates.', '2016-09-19', '10:17:08', '2016-09-19', '10:17:08', 0),
	(62, 56, '1609007', 'Set 4', 10, '2016-09-21', '10:30:00', '12:00:00', 'Please bring ballpen. Re sched ky nag Baha', '2016-09-19', '10:40:22', '2016-09-19', '10:49:22', 1),
	(63, 57, '1608006', 'Set 1', 7, '2016-09-29', '09:00:00', '17:30:00', 'Please bring my heart.', '2016-09-19', '11:01:20', '2016-09-19', '11:32:00', 1),
	(64, 58, '1609009', 'Set 1', 15, '2016-09-21', '15:30:00', '17:30:00', 'Please bring your assignment.\r\n', '2016-09-19', '11:12:16', '2016-09-19', '11:12:16', 0),
	(65, 59, '1609007', 'Set 5', 10, '2016-09-30', '09:30:00', '12:00:00', 'Please bring a friend.', '2016-09-19', '11:38:52', '2016-09-19', '11:38:52', 0);
/*!40000 ALTER TABLE `tbl_schedclass` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_schools
CREATE TABLE IF NOT EXISTS `tbl_schools` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `schoolid` varchar(30) NOT NULL,
  `description` varchar(300) NOT NULL,
  `date_create` date NOT NULL,
  `time_create` time NOT NULL,
  `date_update` date NOT NULL,
  `time_update` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_schools: 8 rows
/*!40000 ALTER TABLE `tbl_schools` DISABLE KEYS */;
INSERT INTO `tbl_schools` (`id`, `schoolid`, `description`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(1, 'USLS', 'University of St. Lasalle', '2016-08-05', '23:27:55', '2016-08-05', '23:27:55'),
	(2, 'Riverside', 'Riverside Medical School', '2016-08-11', '02:24:28', '2016-08-11', '02:24:28'),
	(3, 'UNOR', 'University of Negros Occidental - Recoletos', '2016-08-15', '02:16:48', '2016-08-15', '02:16:48'),
	(4, 'WestNeg', 'West Negros University', '2016-08-15', '02:17:08', '2016-08-15', '02:17:08'),
	(5, 'Other', 'Other School', '2016-09-01', '00:04:53', '2016-09-01', '00:04:53'),
	(6, 'BCC-Bago', 'Bago City College', '2016-09-01', '00:05:57', '2016-09-01', '00:05:57'),
	(7, 'VCC', 'Victorias City College', '2016-09-08', '23:13:51', '2016-09-08', '23:13:51'),
	(8, 'BCC-Bacolod', 'Bacolod City College', '2016-09-08', '23:29:35', '2016-09-08', '23:29:35');
/*!40000 ALTER TABLE `tbl_schools` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_students
CREATE TABLE IF NOT EXISTS `tbl_students` (
  `stu_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `stud_id` varchar(50) NOT NULL,
  `usr_password` varchar(50) NOT NULL,
  `picture` varchar(100) NOT NULL,
  `fname` varchar(180) NOT NULL DEFAULT '',
  `mname` varchar(180) NOT NULL DEFAULT '    ',
  `lname` varchar(180) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `takenum` int(5) NOT NULL,
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `civil` varchar(180) NOT NULL DEFAULT '',
  `email` varchar(180) NOT NULL DEFAULT '',
  `contactnum` varchar(50) NOT NULL DEFAULT '',
  `haddress` varchar(180) NOT NULL DEFAULT '',
  `course` varchar(100) NOT NULL DEFAULT '',
  `yeargrad` varchar(50) NOT NULL DEFAULT '0',
  `school` varchar(100) NOT NULL DEFAULT '',
  `date_create` date NOT NULL DEFAULT '0000-00-00',
  `time_create` time NOT NULL DEFAULT '00:00:00',
  `date_update` date NOT NULL DEFAULT '0000-00-00',
  `time_update` time NOT NULL DEFAULT '00:00:00',
  `final` int(1) NOT NULL DEFAULT '0',
  `passed` int(1) NOT NULL DEFAULT '0',
  `batchid` int(10) NOT NULL DEFAULT '0',
  `enrolled` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`stu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_students: 48 rows
/*!40000 ALTER TABLE `tbl_students` DISABLE KEYS */;
INSERT INTO `tbl_students` (`stu_id`, `stud_id`, `usr_password`, `picture`, `fname`, `mname`, `lname`, `gender`, `takenum`, `dob`, `civil`, `email`, `contactnum`, `haddress`, `course`, `yeargrad`, `school`, `date_create`, `time_create`, `date_update`, `time_update`, `final`, `passed`, `batchid`, `enrolled`) VALUES
	(12, '1600012', '', '1600012.jpg', 'Lancelot ', '    ', 'The Warrior', 'Male', 0, '1995-01-12', 'Single', 'jer@mai.com', '433123', 'Silay', 'Nursing', '2010', 'Riverside', '2016-08-11', '02:26:48', '2016-08-11', '02:26:48', 1, 1, 9, 0),
	(11, '1600009', '', '1600009.jpg', 'Alexander', '    ', 'The Great', 'Male', 0, '1995-03-23', 'Single', 'jer@mai.com', '433123', 'bacolod', 'Nursing', '2010', 'USLS', '2016-07-30', '21:54:37', '2016-07-30', '21:54:37', 1, 1, 9, 0),
	(13, '1600013', '', '1600013.jpg', 'Sam', '    ', 'Topacio', 'Female', 0, '1995-03-23', 'Single', 'jer@mai.com', '0912345', 'Silay', 'Nursing', '2016', 'USLS', '2016-08-15', '01:50:46', '2016-08-15', '01:50:46', 1, 1, 9, 0),
	(14, '1600014', '', '1600014.jpg', 'Rainier Gray', '    ', 'Cawaling', 'Male', 0, '1995-11-01', 'Married', 'jer@mai.com', '0912', 'Taculing', 'CRIM', '2012', 'UNOR', '2016-08-15', '02:18:06', '2016-08-15', '02:18:06', 1, 1, 8, 0),
	(15, '1600015', 'e10adc3949ba59abbe56e057f20f883e', '1600015.jpg', 'Errly Marie', '    ', 'Gaduyon', 'Female', 0, '1995-11-01', 'Single', 'jer@mai.com', '09123', 'Bacolod', 'Nursing', '2012', 'USLS', '2016-08-15', '22:29:09', '2016-08-15', '22:29:09', 1, 1, 9, 0),
	(16, '1600016', '81dc9bdb52d04dc20036dbd8313ed055', '1600016.jpg', 'Matthew', '    ', 'Frias', 'Male', 0, '1995-11-01', 'Married', 'jer@mai.com', '09123', 'La Carlota', 'CRIM', '2015', 'WestNeg', '2016-08-15', '22:32:54', '2016-08-15', '22:32:54', 1, 1, 8, 0),
	(17, '1600017', '202cb962ac59075b964b07152d234b70', '1600017.jpg', 'Manuel', '    ', 'Maghari', 'Male', 0, '1993-11-01', 'Married', 'jer@mai.com', '09123', 'Maao, Bago', 'Agri', '2014', 'USLS', '2016-08-18', '00:32:53', '2016-08-18', '00:32:53', 1, 1, 12, 0),
	(18, '1600018', '202cb962ac59075b964b07152d234b70', '1600018.jpg', 'Edbert', '    ', 'Esteves', 'Male', 1, '1998-11-11', 'Single', 'jer@mai.com', '433123', 'Bacolod', 'CRIM', '2015', 'UNOR', '2016-08-29', '17:11:54', '2016-08-29', '17:11:54', 1, 1, 8, 0),
	(19, '1600019', '202cb962ac59075b964b07152d234b70', '1600019.jpg', 'Alden', '    ', 'Richards', 'Male', 0, '1990-12-25', 'Married', 'jer@mai.com', '433123', 'Manila', 'Nursing', '2010', 'WestNeg', '2016-08-29', '17:13:27', '2016-08-29', '17:13:27', 1, 1, 9, 0),
	(20, '1600020', '202cb962ac59075b964b07152d234b70', '1600020.jpg', 'Anne', '    ', 'Curtis', 'Female', 0, '1989-01-12', 'Single', 'anne@gmail.com', '433123', 'Manila', 'Nursing', '2009', 'Riverside', '2016-08-29', '17:16:11', '2016-08-29', '17:16:11', 1, 1, 9, 0),
	(21, '1600021', '202cb962ac59075b964b07152d234b70', '1600021.jpg', 'EJ', '    ', 'Arnado', 'Male', 1, '1995-07-01', 'Single', 'Ej@gmail.com', '433123', 'Bacolod City', 'CRIM', '2016', 'UNOR', '2016-09-01', '00:12:39', '2016-09-01', '00:12:39', 1, 1, 8, 0),
	(22, '1600022', '202cb962ac59075b964b07152d234b70', '1600022.png', 'Dandy', '    ', 'Deleon', 'Male', 1, '1993-03-31', 'Single', 'Janjanpogi@gmail.com', '09102339230', 'Prk. Dalawidaw Brgy. 16, Bacolod City', 'Nursing', '2015', 'USLS', '2016-09-08', '22:59:39', '2016-09-08', '22:59:39', 1, 1, 9, 0),
	(23, '1600023', '202cb962ac59075b964b07152d234b70', '1600023.png', 'Sydrex Lois', '    ', 'PeÃ±asas', 'Male', 0, '1995-03-23', 'Single', 'loys@gmail.com', '09204665232', 'Brgy. Mansilingan, Bacolod City', 'CRIM', '2016', 'UNOR', '2016-09-08', '23:01:39', '2016-09-08', '23:01:39', 1, 1, 8, 0),
	(24, '1600024', '202cb962ac59075b964b07152d234b70', '1600024.png', 'Michael', '    ', 'Tindoc', 'Male', 0, '1995-04-24', 'Single', 'michael@gmail.com', '4331239', 'Libertad Baybay Bacolod City', 'CRIM', '2016', 'UNOR', '2016-09-08', '23:02:44', '2016-09-08', '23:02:44', 1, 1, 8, 0),
	(25, '1600025', '202cb962ac59075b964b07152d234b70', '1600025.png', 'Jason', '    ', 'Ligaya', 'Male', 0, '1993-09-09', 'Single', 'jason123@gmail.com', '09394567238', 'Prk. Dalawidaw, Brgy. 16, Bacolod City', 'CRIM', '2015', 'BCC-Bago', '2016-09-08', '23:03:58', '2016-09-08', '23:03:58', 1, 1, 8, 0),
	(26, '1600026', '202cb962ac59075b964b07152d234b70', '1600026.png', 'Jomar', '    ', 'Pamposa', 'Male', 1, '1990-09-19', 'Single', 'jomar321@gmail.com', '09497823476', 'Tondo, Manila', 'CRIM', '2010', 'Other', '2016-09-08', '23:05:30', '2016-09-08', '23:05:30', 1, 1, 8, 0),
	(27, '1600027', '202cb962ac59075b964b07152d234b70', '1600027.png', 'Jonald', '    ', 'Pamposa', 'Male', 0, '1993-09-19', 'Single', 'jonald1233@gmail.com', '09103398123', 'Prk. Dalawidaw Brgy. 16 Bacolod City', 'Nursing', '2014', 'Riverside', '2016-09-08', '23:06:36', '2016-09-08', '23:06:36', 1, 1, 9, 0),
	(28, '1600028', '202cb962ac59075b964b07152d234b70', '1600028.png', 'Larry', '    ', 'Burgon', 'Male', 0, '1993-08-08', 'Single', 'larrylarry@gmail.com', '09397978645', 'Silay City', 'CRIM', '2015', 'UNOR', '2016-09-08', '23:07:49', '2016-09-08', '23:07:49', 1, 0, 7, 1),
	(29, '1600029', '202cb962ac59075b964b07152d234b70', '1600029.png', 'Paolo', '    ', 'LightningSun', 'Male', 1, '1995-03-23', 'Single', 'lightningsun@gmail.com', '09952345938', 'Brgy. 16 Bacolod City', 'CRIM', '2016', 'UNOR', '2016-09-08', '23:09:26', '2016-09-08', '23:09:26', 1, 1, 8, 0),
	(30, '1600030', '202cb962ac59075b964b07152d234b70', '1600030.png', 'Bodie', '    ', 'Tibang', 'Male', 0, '1995-03-05', 'Single', 'bodiebodie@gmail.com', '09498973645', 'Brgy. Mansilingan, Bacolod City', 'Nursing', '2015', 'Riverside', '2016-09-08', '23:11:00', '2016-09-08', '23:11:00', 1, 1, 9, 0),
	(31, '1600031', '202cb962ac59075b964b07152d234b70', '1600031.png', 'Brad', '    ', 'Lolik', 'Male', 1, '1985-05-31', 'Married', 'bradbrad@gmail.com', '4331234', 'Prk. Aguila, Brgy. 16', 'Nursing', '2005', 'USLS', '2016-09-08', '23:12:08', '2016-09-08', '23:12:08', 1, 1, 9, 0),
	(32, '1600032', '202cb962ac59075b964b07152d234b70', '1600032.png', 'Mortred', '    ', 'Assasin', 'Female', 1, '1992-07-01', 'Married', 'mortmort@gmail.com', '09492389123', 'Victorias City', 'Agri', '2013', 'VCC', '2016-09-08', '23:15:29', '2016-09-08', '23:15:29', 1, 1, 12, 0),
	(33, '1600033', '202cb962ac59075b964b07152d234b70', '1600033.jpg', 'Blud', '    ', 'Seek', 'Male', 1, '1990-09-30', 'Single', 'bludlud@gmail.com', '4331238', 'Brgy. Banago, Bacolod City', 'Agri', '2012', 'USLS', '2016-09-08', '23:17:51', '2016-09-08', '23:17:51', 1, 1, 12, 0),
	(34, '1600034', '202cb962ac59075b964b07152d234b70', '1600034.jpg', 'Lina', '    ', 'Inverse', 'Female', 0, '1991-07-30', 'Single', 'linlina@gmail.com', '4331234', 'Camela subd. Talisay City', 'Agri', '2012', 'USLS', '2016-09-08', '23:19:44', '2016-09-08', '23:19:44', 1, 1, 12, 0),
	(35, '1600035', '202cb962ac59075b964b07152d234b70', '1600035.jpg', 'Lanaya', '    ', 'Templar', 'Female', 1, '1993-01-01', 'Single', 'temptemp@gmail.com', '433123', 'Prk. Malipayon, Brgy. 35, Bacolod City', 'Agri', '2013', 'USLS', '2016-09-08', '23:21:03', '2016-09-08', '23:21:03', 1, 0, 13, 1),
	(36, '1600036', '202cb962ac59075b964b07152d234b70', '1600036.jpg', 'Thrall', '    ', 'Disruptor', 'Male', 1, '1990-01-02', 'Single', 'thrallalo@gmail.com', '4330978', 'Bacolod City', 'Agri', '2011', 'USLS', '2016-09-08', '23:21:51', '2016-09-08', '23:21:51', 1, 0, 13, 1),
	(37, '1600037', '202cb962ac59075b964b07152d234b70', '1600037.jpg', 'Allen', '    ', 'Iverson', 'Male', 2, '1990-03-12', 'Single', 'aleennn@gmail.com', '4331234', 'Bacolod City', 'Agri', '2011', 'USLS', '2016-09-08', '23:23:44', '2016-09-08', '23:23:44', 1, 1, 12, 0),
	(38, '1600038', '202cb962ac59075b964b07152d234b70', '1600038.jpg', 'Testsuya', '    ', 'Kuroko', 'Male', 0, '1990-03-23', 'Single', 'kurkur@gmail.com', '4331234', 'San Carlos City', 'Agri', '2012', 'USLS', '2016-09-08', '23:24:50', '2016-09-08', '23:24:50', 1, 1, 12, 0),
	(39, '1600039', '202cb962ac59075b964b07152d234b70', '1600039.jpg', 'Taiga', '    ', 'Kagami', 'Male', 2, '1990-04-04', 'Single', 'taitagai@gmail.com', '4331234', 'Bacolod City', 'Agri', '2013', 'USLS', '2016-09-08', '23:25:40', '2016-09-08', '23:25:40', 0, 0, 13, 1),
	(40, '1600040', '979d472a84804b9f647bc185a877a8b5', '1600040.jpg', 'Akame', '    ', 'Gakill', 'Female', 0, '1990-02-02', 'Single', 'akameakame@gmail.com', '4333323', 'Bacolod City', 'Teacher', '2011', 'BCC-Bacolod', '2016-09-08', '23:30:21', '2016-09-08', '23:30:21', 1, 1, 14, 0),
	(41, '1600041', '202cb962ac59075b964b07152d234b70', '1600041.jpg', 'Joanna', '    ', 'Fernandez', 'Female', 1, '1988-02-12', 'Married', 'jojojo@gmail.com', '09398976464', 'Brgy. 16 Bacolod City', 'Agri', '2010', 'USLS', '2016-09-08', '23:33:00', '2016-09-08', '23:33:00', 1, 1, 12, 0),
	(42, '1600042', '202cb962ac59075b964b07152d234b70', '1600042.jpg', 'Jose Carlos', '    ', 'Basco', 'Male', 0, '1995-03-03', 'Single', 'josejose@gmail.com', '09052345067', 'Las Palmas Subd. DoÃ±a Juliana Hts.', 'Teacher', '2015', 'BCC-Bacolod', '2016-09-12', '01:05:31', '2016-09-12', '01:05:31', 1, 0, 15, 1),
	(43, '1600043', '202cb962ac59075b964b07152d234b70', '1600043.jpg', 'Jason', '    ', 'Bating', 'Male', 0, '1995-03-24', 'Married', 'jbating@gmail.com', '09498976545', 'Prk. Bm, Taculing', 'Teacher', '2015', 'UNOR', '2016-09-12', '01:06:25', '2016-09-12', '01:06:25', 1, 0, 15, 1),
	(44, '1600044', '202cb962ac59075b964b07152d234b70', '1600044.jpg', 'Amiel Jun', '    ', 'Danoy', 'Male', 1, '1995-04-16', 'Married', 'adanoy@gmail.com', '09438345232', 'Abas Santos St. Brgy. 39', 'Teacher', '2015', 'UNOR', '2016-09-12', '01:07:55', '2016-09-12', '01:07:55', 1, 1, 14, 0),
	(45, '1600045', '202cb962ac59075b964b07152d234b70', '1600045.jpg', 'Armie', '    ', 'Abellar', 'Female', 0, '1995-06-08', 'Married', 'arm@gmail.com', '4342346', 'Prk. 3, Magsungay', 'Teacher', '2015', 'UNOR', '2016-09-12', '01:09:19', '2016-09-12', '01:09:19', 1, 1, 14, 0),
	(46, '1600046', '202cb962ac59075b964b07152d234b70', '1600046.jpg', 'Kristine Lee', '    ', 'Amante', 'Female', 0, '1995-11-24', 'Single', 'lee2@gmail.com', '4339586', 'Rodriguez Baybay, Brgy. 35', 'Teacher', '2015', 'USLS', '2016-09-12', '01:10:46', '2016-09-12', '01:10:46', 1, 1, 14, 0),
	(47, '1600047', '202cb962ac59075b964b07152d234b70', '1600047.jpg', 'Kimberly', '    ', 'Amiado', 'Female', 0, '1996-05-30', 'Single', 'kimberber@gmail.com', '4332342', 'Prk. Mahili-ugyonon, Brgy. Felisa', 'Teacher', '2016', 'BCC-Bacolod', '2016-09-12', '01:12:26', '2016-09-12', '01:12:26', 1, 1, 14, 0),
	(48, 'TC16048', '202cb962ac59075b964b07152d234b70', 'TC16048.jpg', 'Kristina', 'Undang', 'AvanceÃ±a', 'Female', 0, '1996-02-07', 'Single', 'tintintin@gmail.com', '4332345', 'San Juan St. Brgy 12', 'Teacher', '2016', 'USLS', '2016-09-12', '01:25:35', '2016-09-12', '01:25:35', 1, 1, 14, 0),
	(49, 'TC16049', '202cb962ac59075b964b07152d234b70', 'TC16049.jpg', 'Mary Jane', 'Pontino', 'Chu', 'Female', 0, '1995-02-03', 'Single', 'CHUCHUCHU@gmail.com', '4335343', 'Regent Pearl Subd., Alijis', 'Teacher', '2016', 'UNOR', '2016-09-12', '01:27:27', '2016-09-12', '01:27:27', 1, 1, 14, 0),
	(50, 'TC16050', '202cb962ac59075b964b07152d234b70', 'TC16050.jpg', 'Christeen', 'Deruel', 'De La PeÃ±a', 'Female', 0, '1995-12-22', 'Single', 'CDdele@gmail.com', '7112356', 'Prk. Pilit, Brgy. 6', 'Teacher', '2016', 'BCC-Bacolod', '2016-09-12', '01:28:45', '2016-09-12', '01:28:45', 1, 1, 14, 0),
	(51, 'AG16051', '202cb962ac59075b964b07152d234b70', 'AG16051.jpg', 'Jean', 'Briones', 'Briones', 'Female', 0, '1997-05-05', 'Single', 'jeanjeanjean@gmail.com', '09492736455', 'Bacolod', 'Agri', '2015', 'USLS', '2016-09-17', '13:03:17', '2016-09-17', '13:03:17', 0, 0, 13, 1),
	(52, 'AG16052', '202cb962ac59075b964b07152d234b70', 'AG16052.jpg', 'Nicole', 'Car', 'Cordova', 'Male', 0, '1994-03-03', 'Single', 'nicnicCordo@gmail.com', '09493939233', 'Prk. Mainuswagon Brgy. 17', 'Agri', '2015', 'BCC-Bago', '2016-09-18', '02:12:33', '2016-09-18', '02:12:33', 0, 0, 13, 1),
	(53, 'AG16053', '202cb962ac59075b964b07152d234b70', 'AG16053.jpg', 'Fuzzy', 'Duzzy', 'Wuzzy', 'Male', 0, '1993-09-03', 'Single', 'fuzzWuz@gmail.com', '09992384232', 'Bacolod City', 'Agri', '2013', 'UNOR', '2016-09-18', '02:13:52', '2016-09-18', '02:13:52', 0, 0, 13, 1),
	(54, 'AG16054', '202cb962ac59075b964b07152d234b70', 'AG16054.jpg', 'Doom', 'Didum', 'Badoom', 'Male', 0, '1990-01-01', 'Single', 'bdooom@gmail.com', '09059848234', 'Silay City', 'Agri', '2014', 'WestNeg', '2016-09-18', '02:15:51', '2016-09-18', '02:15:51', 0, 0, 13, 1),
	(55, 'AG16055', '202cb962ac59075b964b07152d234b70', 'AG16055.jpg', 'Therese', 'Thoroso', 'Dumla', 'Female', 0, '1993-09-17', 'Single', 'thoroso@gmail.com', '09089847565', 'Murcia', 'Agri', '2015', 'UNOR', '2016-09-18', '02:18:41', '2016-09-18', '02:18:41', 0, 0, 13, 1),
	(56, 'AG16056', '202cb962ac59075b964b07152d234b70', 'AG16056.jpg', 'Janella', 'Dumdum', 'Brion', 'Female', 1, '1994-03-23', 'Single', 'Ddum@gmail.com', '09493874666', 'Bacolod CIty', 'Agri', '2015', 'UNOR', '2016-09-18', '02:20:21', '2016-09-18', '02:20:21', 0, 0, 13, 1),
	(57, 'AG16057', '202cb962ac59075b964b07152d234b70', 'AG16057.jpg', 'Kristina', 'Manuel', 'Noeel', 'Female', 1, '1995-08-08', 'Single', 'kriskris@gmail.com', '09498767534', 'Bacolod', 'Agri', '2016', 'BCC-Bago', '2016-09-18', '02:21:29', '2016-09-18', '02:21:29', 0, 0, 13, 1),
	(58, 'NU16058', '202cb962ac59075b964b07152d234b70', 'NU16058.jpg', 'Samantha', 'Miranda', 'Morgana', 'Female', 0, '1995-03-23', 'Single', 'samorg@gmail.com', '0908645336', 'Silay City', 'Nursing', '2016', 'Riverside', '2016-09-19', '01:49:08', '2016-09-19', '01:49:08', 0, 0, 10, 1),
	(59, 'NU16059', '81dc9bdb52d04dc20036dbd8313ed055', 'NU16059.jpg', 'John Altair', 'Lao Chua', 'Gensola', 'Male', 0, '1997-04-09', 'Single', 'johnaltair@naver.com', '09222222222', 'Bacolod City', 'Nursing', '2017', 'USLS', '2016-09-19', '10:37:19', '2016-09-19', '10:37:19', 0, 0, 10, 1),
	(60, 'NU16060', 'b904fb255582df7f37997daa0fb16cf7', 'NU16060.jpg', 'Khrisna', 'Tan', 'Elumba', 'Female', 0, '1990-01-04', 'Single', 'email@gmail.com', '09234567891', 'Bacolod City', 'Nursing', '2011', 'USLS', '2016-09-19', '10:45:41', '2016-09-19', '10:45:41', 0, 0, 10, 1),
	(61, 'CR16061', '202cb962ac59075b964b07152d234b70', 'CR16061.jpg', 'Clint', 'Badillo', 'Jalando-on', 'Male', 0, '1995-04-14', 'Single', 'clintjalandoon10@gmail.com', '09989781322', 'Bacolod City', 'CRIM', '2016', 'BCC-Bago', '2016-09-19', '10:58:45', '2016-09-19', '10:58:45', 0, 0, 7, 1),
	(62, 'TC16062', 'd48be77afc1f5bef94d664adac2a046f', 'TC16062.jpg', 'Catherine', 'Flores', 'Caunga', 'Female', 0, '1995-06-09', 'Single', 'cathcaunga@gmail.com', '09327817891', 'Bacolod City', 'Teacher', '2011', 'USLS', '2016-09-19', '11:07:51', '2016-09-19', '11:07:51', 0, 0, 15, 1),
	(63, 'CR16063', '202cb962ac59075b964b07152d234b70', 'CR16063.jpg', 'Daniel', 'Isidro', 'Yared', 'Male', 1, '1996-08-03', 'Single', 'vizzledrix@gmail.com', '09466433568', 'Somewhere', 'CRIM', '2017', 'Other', '2016-09-19', '11:28:33', '2016-09-19', '11:28:33', 0, 0, 7, 1),
	(64, 'NU16064', '440b447e404686133097665c9c4022ab', 'NU16064.jpg', 'Yearna', 'Camon', 'Gregorio', 'Female', 0, '2016-09-01', 'Single', 'yearnagregorio@gmail.com', '09777151610', 'La Carlota City', 'Nursing', '2016', 'USLS', '2016-09-19', '11:36:16', '2016-09-19', '11:36:16', 0, 0, 10, 1);
/*!40000 ALTER TABLE `tbl_students` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_subjects
CREATE TABLE IF NOT EXISTS `tbl_subjects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject_id` varchar(30) NOT NULL,
  `description` varchar(30) NOT NULL,
  `examid` varchar(30) NOT NULL,
  `course` varchar(30) NOT NULL,
  `date_create` date NOT NULL,
  `time_create` time NOT NULL,
  `date_update` date NOT NULL,
  `time_update` time NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_subjects: 20 rows
/*!40000 ALTER TABLE `tbl_subjects` DISABLE KEYS */;
INSERT INTO `tbl_subjects` (`id`, `subject_id`, `description`, `examid`, `course`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(3, 'Set 1', 'Refresher', 'ALE', 'Agri', '2016-08-03', '21:58:40', '2016-08-03', '21:58:40'),
	(4, 'Set 1', 'Refresher', 'NLE', 'Nursing', '2016-08-03', '21:58:56', '2016-08-03', '21:58:56'),
	(5, 'Set 1', 'Refresher', 'CLE', 'CRIM', '2016-08-03', '22:36:50', '2016-08-03', '22:36:50'),
	(10, 'Set 2', 'Set 2', 'CLE', 'CRIM', '2016-09-07', '20:24:04', '2016-09-07', '20:24:04'),
	(9, 'Set 2', 'Set 2', 'ALE', 'Agri', '2016-09-07', '20:23:28', '2016-09-07', '20:23:28'),
	(11, 'Set 2', 'Set 2', 'LET', 'Teacher', '2016-09-07', '20:24:24', '2016-09-07', '20:24:24'),
	(12, 'Set 2', 'Set 2', 'NLE', 'Nursing', '2016-09-07', '20:24:36', '2016-09-07', '20:24:36'),
	(13, 'Set 1', 'Refresher', 'LET', 'Teacher', '2016-09-07', '20:25:19', '2016-09-07', '20:25:19'),
	(14, 'Set 3', 'Set 3', 'CLE', 'CRIM', '2016-09-12', '01:36:32', '2016-09-12', '01:36:32'),
	(15, 'Set 4', 'Set 4', 'CLE', 'CRIM', '2016-09-12', '01:36:45', '2016-09-12', '01:36:45'),
	(16, 'Set 5', 'Set 5', 'CLE', 'CRIM', '2016-09-12', '01:36:55', '2016-09-12', '01:36:55'),
	(17, 'Set 3', 'Set 3', 'NLE', 'Nursing', '2016-09-12', '01:44:40', '2016-09-12', '01:44:40'),
	(18, 'Set 3', 'Set 3', 'ALE', 'Agri', '2016-09-15', '17:08:42', '2016-09-15', '17:08:42'),
	(19, 'Set 4', 'Set 4', 'ALE', 'Agri', '2016-09-15', '17:08:51', '2016-09-15', '17:08:51'),
	(20, 'Set 5', 'Set 5', 'ALE', 'Agri', '2016-09-15', '17:09:02', '2016-09-15', '17:09:02'),
	(21, 'Set 4', 'Set 4', 'NLE', 'Nursing', '2016-09-15', '17:09:29', '2016-09-15', '17:09:29'),
	(22, 'Set 5', 'Set 5', 'NLE', 'Nursing', '2016-09-15', '17:09:37', '2016-09-15', '17:09:37'),
	(23, 'Set 3', 'Set 3', 'LET', 'Teacher', '2016-09-15', '17:09:51', '2016-09-15', '17:09:51'),
	(24, 'Set 4', 'Set 4', 'LET', 'Teacher', '2016-09-15', '17:10:03', '2016-09-15', '17:10:03'),
	(25, 'Set 5', 'Set 5', 'LET', 'Teacher', '2016-09-15', '17:10:14', '2016-09-15', '17:10:14');
/*!40000 ALTER TABLE `tbl_subjects` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_teachers
CREATE TABLE IF NOT EXISTS `tbl_teachers` (
  `tea_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `tidnum` varchar(50) NOT NULL,
  `usr_password` varchar(50) NOT NULL DEFAULT '',
  `picture` varchar(100) NOT NULL,
  `fname` varchar(180) NOT NULL DEFAULT '',
  `lname` varchar(180) NOT NULL DEFAULT '',
  `gender` varchar(10) NOT NULL DEFAULT '',
  `dob` date NOT NULL DEFAULT '0000-00-00',
  `civil` varchar(180) NOT NULL DEFAULT '',
  `email` varchar(180) NOT NULL DEFAULT '',
  `contactnum` varchar(50) NOT NULL DEFAULT '',
  `haddress` varchar(180) NOT NULL DEFAULT '',
  `field` varchar(100) NOT NULL DEFAULT '',
  `school` varchar(100) NOT NULL DEFAULT '',
  `date_create` date NOT NULL DEFAULT '0000-00-00',
  `time_create` time NOT NULL DEFAULT '00:00:00',
  `date_update` date NOT NULL DEFAULT '0000-00-00',
  `time_update` time NOT NULL DEFAULT '00:00:00',
  PRIMARY KEY (`tea_id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_teachers: 5 rows
/*!40000 ALTER TABLE `tbl_teachers` DISABLE KEYS */;
INSERT INTO `tbl_teachers` (`tea_id`, `tidnum`, `usr_password`, `picture`, `fname`, `lname`, `gender`, `dob`, `civil`, `email`, `contactnum`, `haddress`, `field`, `school`, `date_create`, `time_create`, `date_update`, `time_update`) VALUES
	(8, '1609008', '202cb962ac59075b964b07152d234b70', '1609008.jpg', 'Niel', 'Bunda', 'Male', '1985-03-03', 'Married', 'leinlein@gmail.com', '4331234', 'Homesite', 'CRIM', 'University of Negros Occidental - Recoletos', '2016-09-09', '21:01:27', '2016-09-09', '21:01:27'),
	(7, '1609007', '202cb962ac59075b964b07152d234b70', '1609007.jpg', 'Charina', 'Puentevella', 'Female', '1985-07-24', 'Single', 'chacha@gmail.com', '4331234', 'Iloilo', 'Nursing', 'University Of. St. Lasalle - Bacolod', '2016-09-09', '20:58:55', '2016-09-09', '20:58:55'),
	(6, '1608006', '202cb962ac59075b964b07152d234b70', '1608006.jpg', 'Bill', 'Dela Fuente', 'Male', '1980-08-20', 'Married', 'jer@mai.com', '09123', '', 'CRIM', 'University Of St. La Salle - Bacolod', '2016-08-19', '22:33:23', '2016-08-19', '22:33:23'),
	(9, '1609009', '202cb962ac59075b964b07152d234b70', '1609009.jpg', 'Mae', 'Jallorina', 'Female', '1985-08-18', 'Married', 'maemae@gmail.com', '4331234', 'Bacolod City', 'Teacher', 'University Of St. Lasalle - Bacolod', '2016-09-09', '21:02:50', '2016-09-09', '21:02:50'),
	(10, '1609010', '202cb962ac59075b964b07152d234b70', '1609010.jpg', 'Ice', 'Arcenal', 'Male', '1985-08-28', 'Single', 'iceice@gmail.com', '4331234', 'Bacolod', 'Agri', 'West Visayas State University', '2016-09-09', '21:03:50', '2016-09-09', '21:03:50');
/*!40000 ALTER TABLE `tbl_teachers` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_teachtime
CREATE TABLE IF NOT EXISTS `tbl_teachtime` (
  `time_id` int(11) NOT NULL AUTO_INCREMENT,
  `tidnum` varchar(50) NOT NULL DEFAULT '0',
  `date` date NOT NULL DEFAULT '0000-00-00',
  `personnel` varchar(50) NOT NULL DEFAULT '0000-00-00',
  `classid` int(5) NOT NULL,
  `hours` int(2) NOT NULL,
  PRIMARY KEY (`time_id`),
  KEY `tidnum` (`tidnum`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_teachtime: ~1 rows (approximately)
/*!40000 ALTER TABLE `tbl_teachtime` DISABLE KEYS */;
INSERT INTO `tbl_teachtime` (`time_id`, `tidnum`, `date`, `personnel`, `classid`, `hours`) VALUES
	(7, '1608006', '2016-09-05', 'admin', 9, 3),
	(8, '1609010', '2016-09-07', 'admin', 17, 3),
	(9, '1608006', '2016-09-16', 'admin', 9, 2);
/*!40000 ALTER TABLE `tbl_teachtime` ENABLE KEYS */;


-- Dumping structure for table rgo_db.tbl_users
CREATE TABLE IF NOT EXISTS `tbl_users` (
  `usr_id` int(4) unsigned NOT NULL AUTO_INCREMENT,
  `usr_idnum` varchar(100) NOT NULL DEFAULT '',
  `usr_password` varchar(100) NOT NULL DEFAULT '',
  `usr_date_added` date NOT NULL DEFAULT '0000-00-00',
  `usr_time_added` time NOT NULL DEFAULT '00:00:00',
  `usr_date_modified` date NOT NULL DEFAULT '0000-00-00',
  `usr_time_modified` time NOT NULL DEFAULT '00:00:00',
  `usr_position` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`usr_id`)
) ENGINE=MyISAM AUTO_INCREMENT=75 DEFAULT CHARSET=latin1;

-- Dumping data for table rgo_db.tbl_users: 55 rows
/*!40000 ALTER TABLE `tbl_users` DISABLE KEYS */;
INSERT INTO `tbl_users` (`usr_id`, `usr_idnum`, `usr_password`, `usr_date_added`, `usr_time_added`, `usr_date_modified`, `usr_time_modified`, `usr_position`) VALUES
	(23, 'Jackwander', '202cb962ac59075b964b07152d234b70', '2016-08-22', '22:40:40', '2016-08-22', '22:40:40', 'Personnel'),
	(13, '1600012', '81dc9bdb52d04dc20036dbd8313ed055', '2016-08-11', '02:26:48', '2016-08-11', '02:26:48', 'Student'),
	(14, '1600013', '81dc9bdb52d04dc20036dbd8313ed055', '2016-08-15', '01:50:46', '2016-08-15', '01:50:46', 'Student'),
	(15, '1600014', '81dc9bdb52d04dc20036dbd8313ed055', '2016-08-15', '02:18:06', '2016-08-15', '02:18:06', 'Student'),
	(16, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '0000-00-00', '00:00:00', '0000-00-00', '00:00:00', 'Admin'),
	(17, '1600015', 'e10adc3949ba59abbe56e057f20f883e', '2016-08-15', '22:29:09', '2016-08-15', '22:29:09', 'Student'),
	(18, '1600016', '81dc9bdb52d04dc20036dbd8313ed055', '2016-08-15', '22:32:54', '2016-08-15', '22:32:54', 'Student'),
	(19, '1600017', '202cb962ac59075b964b07152d234b70', '2016-08-18', '00:32:53', '2016-08-18', '00:32:53', 'Student'),
	(48, '1609007', '202cb962ac59075b964b07152d234b70', '2016-09-09', '20:58:55', '2016-09-09', '20:58:55', 'Teacher'),
	(11, '1600009', '202cb962ac59075b964b07152d234b70', '2016-07-30', '02:07:45', '2016-07-30', '02:07:45', 'Student'),
	(22, '1608006', '202cb962ac59075b964b07152d234b70', '2016-08-19', '22:33:23', '2016-08-19', '22:33:23', 'Teacher'),
	(24, '1600018', '202cb962ac59075b964b07152d234b70', '2016-08-29', '17:11:54', '2016-08-29', '17:11:54', 'Student'),
	(25, '1600019', '202cb962ac59075b964b07152d234b70', '2016-08-29', '17:13:27', '2016-08-29', '17:13:27', 'Student'),
	(26, '1600020', '202cb962ac59075b964b07152d234b70', '2016-08-29', '17:16:11', '2016-08-29', '17:16:11', 'Student'),
	(27, '1600021', '202cb962ac59075b964b07152d234b70', '2016-09-01', '00:12:39', '2016-09-01', '00:12:39', 'Student'),
	(28, '1600022', '202cb962ac59075b964b07152d234b70', '2016-09-08', '22:59:39', '2016-09-08', '22:59:39', 'Student'),
	(29, '1600023', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:01:39', '2016-09-08', '23:01:39', 'Student'),
	(30, '1600024', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:02:44', '2016-09-08', '23:02:44', 'Student'),
	(31, '1600025', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:03:58', '2016-09-08', '23:03:58', 'Student'),
	(32, '1600026', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:05:30', '2016-09-08', '23:05:30', 'Student'),
	(33, '1600027', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:06:36', '2016-09-08', '23:06:36', 'Student'),
	(34, '1600028', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:07:49', '2016-09-08', '23:07:49', 'Student'),
	(35, '1600029', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:09:26', '2016-09-08', '23:09:26', 'Student'),
	(36, '1600030', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:11:00', '2016-09-08', '23:11:00', 'Student'),
	(37, '1600031', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:12:08', '2016-09-08', '23:12:08', 'Student'),
	(38, '1600032', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:15:29', '2016-09-08', '23:15:29', 'Student'),
	(39, '1600033', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:17:51', '2016-09-08', '23:17:51', 'Student'),
	(40, '1600034', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:19:44', '2016-09-08', '23:19:44', 'Student'),
	(41, '1600035', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:21:03', '2016-09-08', '23:21:03', 'Student'),
	(42, '1600036', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:21:51', '2016-09-08', '23:21:51', 'Student'),
	(43, '1600037', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:23:44', '2016-09-08', '23:23:44', 'Student'),
	(44, '1600038', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:24:50', '2016-09-08', '23:24:50', 'Student'),
	(45, '1600039', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:25:40', '2016-09-08', '23:25:40', 'Student'),
	(46, '1600040', '979d472a84804b9f647bc185a877a8b5', '2016-09-08', '23:30:21', '2016-09-08', '23:30:21', 'Student'),
	(47, '1600041', '202cb962ac59075b964b07152d234b70', '2016-09-08', '23:33:00', '2016-09-08', '23:33:00', 'Student'),
	(49, '1609008', '202cb962ac59075b964b07152d234b70', '2016-09-09', '21:01:27', '2016-09-09', '21:01:27', 'Teacher'),
	(50, '1609009', '202cb962ac59075b964b07152d234b70', '2016-09-09', '21:02:50', '2016-09-09', '21:02:50', 'Teacher'),
	(51, '1609010', '202cb962ac59075b964b07152d234b70', '2016-09-09', '21:03:50', '2016-09-09', '21:03:50', 'Teacher'),
	(52, '1600042', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:05:31', '2016-09-12', '01:05:31', 'Student'),
	(53, '1600043', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:06:25', '2016-09-12', '01:06:25', 'Student'),
	(54, '1600044', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:07:55', '2016-09-12', '01:07:55', 'Student'),
	(55, '1600045', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:09:19', '2016-09-12', '01:09:19', 'Student'),
	(56, '1600046', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:10:46', '2016-09-12', '01:10:46', 'Student'),
	(57, '1600047', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:12:26', '2016-09-12', '01:12:26', 'Student'),
	(58, 'TC16048', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:25:35', '2016-09-12', '01:25:35', 'Student'),
	(59, 'TC16049', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:27:27', '2016-09-12', '01:27:27', 'Student'),
	(60, 'TC16050', '202cb962ac59075b964b07152d234b70', '2016-09-12', '01:28:45', '2016-09-12', '01:28:45', 'Student'),
	(61, 'AG16051', '202cb962ac59075b964b07152d234b70', '2016-09-17', '13:03:17', '2016-09-17', '13:03:17', 'Student'),
	(62, 'AG16052', '202cb962ac59075b964b07152d234b70', '2016-09-18', '02:12:33', '2016-09-18', '02:12:33', 'Student'),
	(63, 'AG16053', '202cb962ac59075b964b07152d234b70', '2016-09-18', '02:13:52', '2016-09-18', '02:13:52', 'Student'),
	(64, 'AG16054', '202cb962ac59075b964b07152d234b70', '2016-09-18', '02:15:51', '2016-09-18', '02:15:51', 'Student'),
	(65, 'AG16055', '202cb962ac59075b964b07152d234b70', '2016-09-18', '02:18:41', '2016-09-18', '02:18:41', 'Student'),
	(66, 'AG16056', '202cb962ac59075b964b07152d234b70', '2016-09-18', '02:20:21', '2016-09-18', '02:20:21', 'Student'),
	(67, 'AG16057', '202cb962ac59075b964b07152d234b70', '2016-09-18', '02:21:29', '2016-09-18', '02:21:29', 'Student'),
	(68, 'NU16058', '202cb962ac59075b964b07152d234b70', '2016-09-19', '01:49:08', '2016-09-19', '01:49:08', 'Student'),
	(69, 'NU16059', '81dc9bdb52d04dc20036dbd8313ed055', '2016-09-19', '10:37:19', '2016-09-19', '10:37:19', 'Student'),
	(70, 'NU16060', 'b904fb255582df7f37997daa0fb16cf7', '2016-09-19', '10:45:41', '2016-09-19', '10:45:41', 'Student'),
	(71, 'CR16061', '202cb962ac59075b964b07152d234b70', '2016-09-19', '10:58:45', '2016-09-19', '10:58:45', 'Student'),
	(72, 'TC16062', 'd48be77afc1f5bef94d664adac2a046f', '2016-09-19', '11:07:51', '2016-09-19', '11:07:51', 'Student'),
	(73, 'CR16063', '202cb962ac59075b964b07152d234b70', '2016-09-19', '11:28:33', '2016-09-19', '11:28:33', 'Student'),
	(74, 'NU16064', '440b447e404686133097665c9c4022ab', '2016-09-19', '11:36:16', '2016-09-19', '11:36:16', 'Student');
/*!40000 ALTER TABLE `tbl_users` ENABLE KEYS */;


-- Dumping structure for table rgo_db.teacher_tbl
CREATE TABLE IF NOT EXISTS `teacher_tbl` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL DEFAULT '0',
  `password` varchar(50) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;

-- Dumping data for table rgo_db.teacher_tbl: ~0 rows (approximately)
/*!40000 ALTER TABLE `teacher_tbl` DISABLE KEYS */;
INSERT INTO `teacher_tbl` (`id`, `username`, `password`) VALUES
	(1, 'leemark', 'leemark');
/*!40000 ALTER TABLE `teacher_tbl` ENABLE KEYS */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
