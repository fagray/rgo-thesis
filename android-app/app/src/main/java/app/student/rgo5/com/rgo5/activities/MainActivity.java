package app.student.rgo5.com.rgo5.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import app.student.rgo5.com.rgo5.objects.User;
import app.student.rgo5.com.rgo5.R;

public class MainActivity extends Activity implements View.OnClickListener {

    public static final String EXTRA_USER_ID = "user_id";

    TextView tvWelcome;
    ImageView imgProfile, imgGrades, imgSchedule, imgAnnouncement, imageButtonLogout;
    String user_id, userName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);

        Bundle extras = getIntent().getExtras();
        setContentView(R.layout.activity_main);

        user_id = extras.get(EXTRA_USER_ID).toString();
        userName = extras.get("user_name").toString();

        Log.e("user id: ", user_id);

        imageButtonLogout = (ImageView) findViewById(R.id.iamgeButtonLogout);
        imgAnnouncement = (ImageView) findViewById(R.id.imageView_announcement);
        imgProfile = (ImageView) findViewById(R.id.imageView_profile);
        imgGrades = (ImageView) findViewById(R.id.imageView_grades);
        imgSchedule = (ImageView) findViewById(R.id.imageView_course);
        tvWelcome = (TextView) findViewById(R.id.tv_welcome);
        tvWelcome.setText("Welcome " + userName);

        imgProfile.setOnClickListener(this);
        imgAnnouncement.setOnClickListener(this);
        imgGrades.setOnClickListener(this);
        imgSchedule.setOnClickListener(this);
        imageButtonLogout.setOnClickListener(this);


    }

    public static void runMainActivity(Activity me, User user) {
        Intent intent = new Intent(me, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(EXTRA_USER_ID, user.stud_id);
        intent.putExtra("user_name", user.fname);
        me.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.imageView_announcement:
                YoYo.with(Techniques.Pulse).duration(100).playOn(imgAnnouncement);
                Intent intent_announcement = new Intent(MainActivity.this, AnnouncementActivity.class);
                intent_announcement.putExtra(EXTRA_USER_ID, user_id);
                startActivity(intent_announcement);
                break;

            case R.id.imageView_profile:
                YoYo.with(Techniques.Pulse).duration(100).playOn(imgProfile);
                Intent intent_profile = new Intent(MainActivity.this, ProfileActivity.class);
                intent_profile.putExtra(EXTRA_USER_ID, user_id);
                startActivity(intent_profile);
                break;

            case R.id.imageView_grades:
                YoYo.with(Techniques.Pulse).duration(100).playOn(imgGrades);
                Intent intent_grades = new Intent(MainActivity.this, GradesActivity.class);
                intent_grades.putExtra(EXTRA_USER_ID, user_id);
                startActivity(intent_grades);
                break;

            case R.id.imageView_course:
                YoYo.with(Techniques.Pulse).duration(100).playOn(imgSchedule);
                Intent intent_course = new Intent(MainActivity.this, CourseActivity.class);
                intent_course.putExtra(EXTRA_USER_ID, user_id);
                startActivity(intent_course);
                break;

            case R.id.iamgeButtonLogout:
                YoYo.with(Techniques.Pulse).duration(100).playOn(imageButtonLogout);
                new AlertDialog.Builder(this)
                        .setIcon(android.R.drawable.ic_menu_close_clear_cancel)
                        .setTitle("Logout")
                        .setMessage("Are you sure you want to logout?")
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                        {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                logout();
                            }

                        })
                        .setNegativeButton("No", null)
                        .show();
                break;
        }
    }

    private void logout(){
        Intent intent_logout = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent_logout);
        finish();
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(android.R.drawable.ic_menu_close_clear_cancel)
                .setTitle("Exit")
                .setMessage("Would you like to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
}
