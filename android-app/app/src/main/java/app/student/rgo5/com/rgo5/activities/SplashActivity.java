package app.student.rgo5.com.rgo5.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import app.student.rgo5.com.rgo5.R;

/**
 * Created by User on 8/24/2016.
 */
public class SplashActivity extends Activity{

    ImageView imgLogo;
    TextView tvAppTitle, tvTagLine;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash);

        imgLogo = (ImageView) findViewById(R.id.imageView_logo);
        tvAppTitle = (TextView) findViewById(R.id.app_title);
        tvTagLine = (TextView) findViewById(R.id.app_tagline);
        YoYo.with(Techniques.FadeIn).duration(5000).playOn(imgLogo);
        YoYo.with(Techniques.FadeIn).duration(5000).playOn(tvAppTitle);
        YoYo.with(Techniques.FadeIn).duration(5000).playOn(tvTagLine);
        thread.start();

    }

    Thread thread = new Thread(){
        public void run(){
            try {
                sleep(5000);

                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                startActivity(intent);

            } catch (InterruptedException e){
                e.printStackTrace();
            } finally {
                finish();
            }
        }

    };

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }
}
