package app.student.rgo5.com.rgo5.objects;

/**
 * Created by User on 8/25/2016.
 */
public class User {
    public String stud_id;
    public String usr_password;
    public String email;
    public String fname;
    public String lname;
    public String picture;
    public String haddress;
    public String gender;
    public String contactnum;
    public String coursecode;
    public String description;
}
