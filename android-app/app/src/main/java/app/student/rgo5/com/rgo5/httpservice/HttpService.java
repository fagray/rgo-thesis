package app.student.rgo5.com.rgo5.httpservice;

import java.util.List;

import app.student.rgo5.com.rgo5.objects.Announcement;
import app.student.rgo5.com.rgo5.objects.Grade;
import app.student.rgo5.com.rgo5.objects.User;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by User on 8/25/2016.
 */
public interface HttpService {
    @FormUrlEncoded
    @POST("api/login.php")
    Call<User> login(@Field("stud_id") String userID, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/student-profile.php")
    Call<User> getUser(@Field("student_id") String userID);

    @GET("api/announcements.php")
    Call<List<Announcement>> getAnnouncement();

    @FormUrlEncoded
    @POST("api/student-grades.php")
    Call<List<Grade>> getGrades(@Field("student_id") String userID);

}

