package app.student.rgo5.com.rgo5.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;

import app.student.rgo5.com.rgo5.R;
import app.student.rgo5.com.rgo5.dynamicstrings.DynamicStrings;
import app.student.rgo5.com.rgo5.httpservice.HttpService;
import app.student.rgo5.com.rgo5.objects.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 8/24/2016.
 */
public class LoginActivity extends Activity {
    EditText etIDNumber, etPassword;
    Button btnLogin;
    User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        etIDNumber = (EditText) findViewById(R.id.idNumber);
        etPassword = (EditText) findViewById(R.id.password);
        btnLogin = (Button) findViewById(R.id.btnLogin);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String username = etIDNumber.getText().toString();
                final String password = etPassword.getText().toString();
                YoYo.with(Techniques.Pulse).duration(200).playOn(btnLogin);
                if (TextUtils.isEmpty(username)) {
                    YoYo.with(Techniques.Shake).duration(500).playOn(etIDNumber);
                    etIDNumber.setError("ID Number cannot be empty!");
                }
                if (TextUtils.isEmpty(password)) {
                    YoYo.with(Techniques.Shake).duration(500).playOn(etPassword);
                    etPassword.setError("Password cannot be empty!");
                }
                if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
                    return;
                }
                if (!isNetworkConnected()) {
                    Toast.makeText(LoginActivity.this, "Please check you internet connection.", Toast.LENGTH_LONG).show();
                } else {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {

                            Login(etIDNumber.getText().toString(), etPassword.getText().toString());

                        }
                    }).start();
                    btnLogin.setEnabled(false);
                    etIDNumber.setEnabled(false);
                    etPassword.setEnabled(false);

                }


            }
        });

    }

    public void Login(String username, String password) {
        Log.d("idNumber: ", username);

        try {
            //Retrofit retrofit = new Retrofit.Builder().baseUrl("http://192.168.0.12/rgo-thesis/").addConverterFactory(GsonConverterFactory.create()).build();
            Retrofit retrofit = new Retrofit.Builder().baseUrl(DynamicStrings.BASE_URL_OFFICE).addConverterFactory(GsonConverterFactory.create()).build();

            HttpService service = retrofit.create(HttpService.class);
            Call<User> getUser = service.login(username, password);

            User user = getUser.execute().body();

                    MainActivity.runMainActivity(LoginActivity.this, user);
                    finish();

            //Log.e("User:", user.stud_id + " " + user.usr_password + " " + user.email);


        } catch (Exception e) {
            LoginActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    //androidUtils.alert("Username and password did not match.");
                    Toast.makeText(LoginActivity.this, "User ID and password did not match. Please try again.", Toast.LENGTH_LONG).show();
                    btnLogin.setEnabled(true);
                    etIDNumber.setEnabled(true);
                    etPassword.setEnabled(true);
                }
            });
            //Toast.makeText(LoginActivity.this, "User ID and password did not match. Please try again.", Toast.LENGTH_LONG).show();
        }
        ;
    }

    private boolean isNetworkConnected() {

        ConnectivityManager cm = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null;
    }

    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this)
                .setIcon(R.drawable.logout)
                .setTitle("Exit")
                .setMessage("Would you like to exit?")
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }

                })
                .setNegativeButton("No", null)
                .show();
    }
    @Override
    protected void onResume() {
        btnLogin.setEnabled(true);
        etIDNumber.setEnabled(true);
        etPassword.setEnabled(true);
        etIDNumber.requestFocus();
        super.onResume();
    }
}
