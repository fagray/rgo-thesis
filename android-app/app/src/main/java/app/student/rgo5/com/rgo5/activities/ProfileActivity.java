package app.student.rgo5.com.rgo5.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.squareup.picasso.Picasso;

import app.student.rgo5.com.rgo5.R;
import app.student.rgo5.com.rgo5.dynamicstrings.DynamicStrings;
import app.student.rgo5.com.rgo5.httpservice.HttpService;
import app.student.rgo5.com.rgo5.objects.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 8/24/2016.
 */
public class ProfileActivity extends Activity {
    public static final String EXTRA_USER_ID = "user_id";
    String user_id;
    ImageView imgUserPhoto;
    TextView tvFname, tvLname, tvIDnumber, tvEmail, tvUsername, tvCity, tvGender, tvContact;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.profile);

        user_id = extras.get(EXTRA_USER_ID).toString();

        Log.e("USER ID:", user_id);

        imgUserPhoto = (ImageView) findViewById(R.id.imageView_user_photo);
        tvFname = (TextView) findViewById(R.id.tv_fname);
        //tvLname = (TextView) findViewById(R.id.tv_lname);
        tvIDnumber = (TextView) findViewById(R.id.tv_id_number);
        tvEmail = (TextView) findViewById(R.id.tv_prof_email);
        tvUsername = (TextView) findViewById(R.id.tv_prof_username);
        tvGender = (TextView) findViewById(R.id.tv_prof_gender);
        tvCity = (TextView) findViewById(R.id.tv_prof_city);
        tvContact = (TextView) findViewById(R.id.tv_prof_contact);

        getUser(user_id);
    }


    public void getUser(String userid) {
        Log.d("idNumber: ", userid);

        try {
            //Retrofit retrofit = new Retrofit.Builder().baseUrl("http://192.168.0.12/rgo-thesis/").addConverterFactory(GsonConverterFactory.create()).build();
            Retrofit retrofit = new Retrofit.Builder().baseUrl(DynamicStrings.BASE_URL_OFFICE).addConverterFactory(GsonConverterFactory.create()).build();

            HttpService service = retrofit.create(HttpService.class);
            Call<User> getUser = service.getUser(userid);

            User user = getUser.execute().body();

            tvFname.setText(user.fname + " " + user.lname);
            //tvLname.setText(user.lname);
            tvIDnumber.setText("User ID: "+ user.stud_id);
            tvEmail.setText(user.email);
            tvGender.setText(user.gender);
            tvCity.setText(user.haddress);
            tvContact.setText(user.contactnum);
            tvUsername.setText(user.fname + " " + user.lname);

            YoYo.with(Techniques.BounceInLeft).playOn(tvFname);

            Picasso.with(this).load(DynamicStrings.BASE_URL_OFFICE + "student/picture/" + user.picture).resize(200, 200).into(imgUserPhoto);

            //Log.d("IMAGE URL: ", DynamicStrings.BASE_URL + "student/picture/" + user.picture);

            //Log.e("User:", user.stud_id + " " + user.fname + " " + user.lname);

        } catch (Exception e) {
            Log.e("Login: ", "" + e);
        }
        ;
    }

}
