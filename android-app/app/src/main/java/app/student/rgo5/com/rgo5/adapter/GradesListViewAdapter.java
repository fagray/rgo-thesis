package app.student.rgo5.com.rgo5.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.student.rgo5.com.rgo5.R;
import app.student.rgo5.com.rgo5.dynamicstrings.DynamicStrings;
import app.student.rgo5.com.rgo5.objects.Announcement;
import app.student.rgo5.com.rgo5.objects.Grade;

/**
 * Created by User on 8/26/2016.
 */
public class GradesListViewAdapter extends ArrayAdapter<Grade> {

    Context context;

    public GradesListViewAdapter(Context context, int resourceId, List<Grade> items) {
        super(context, R.layout.grade_list_item, items);
        this.context = context;


    }

    class ViewHolder {
        TextView set;
        TextView date;
        TextView grade;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;

        Grade grade = getItem(position);


            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.grade_list_item, null);
                //convertView = mInflater.inflate(R.layout.list_item, parent);
                holder = new ViewHolder();
                holder.set = (TextView) convertView.findViewById(R.id.tv_grade_set);
                holder.date = (TextView) convertView.findViewById(R.id.tv_grade_date);
                holder.grade = (TextView) convertView.findViewById(R.id.tv_grade_grade);
                convertView.setTag(holder);
            } else

                holder = (ViewHolder) convertView.getTag();
            holder.set.setText(grade.subjectid);
            holder.date.setText(grade.date_create);
            holder.grade.setText(grade.grade + "%");

            return convertView;



    }

}