package app.student.rgo5.com.rgo5.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import app.student.rgo5.com.rgo5.R;
import app.student.rgo5.com.rgo5.dynamicstrings.DynamicStrings;
import app.student.rgo5.com.rgo5.httpservice.HttpService;
import app.student.rgo5.com.rgo5.objects.User;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 8/24/2016.
 */
public class CourseActivity extends Activity{
    public static final String EXTRA_USER_ID = "user_id";
    String user_id;
    ImageView imgUserPhoto;
    TextView tvFname, tvIDnumber, tvCourse, tvCourseDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle extras = getIntent().getExtras();
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.course);

        user_id = extras.get(EXTRA_USER_ID).toString();

        imgUserPhoto = (ImageView) findViewById(R.id.imageView_cor_user_photo);
        tvFname = (TextView) findViewById(R.id.tv_cor_fname);
        tvIDnumber = (TextView) findViewById(R.id.tv_id_number);
        tvCourse = (TextView) findViewById(R.id.tv_cor_course);
        tvCourseDesc = (TextView) findViewById(R.id.tv_cor_desc);

        getUser(user_id);


    }
    public void getUser(String userid) {
        Log.d("idNumber: ", userid);

        try {
            //Retrofit retrofit = new Retrofit.Builder().baseUrl("http://192.168.0.12/rgo-thesis/").addConverterFactory(GsonConverterFactory.create()).build();
            Retrofit retrofit = new Retrofit.Builder().baseUrl(DynamicStrings.BASE_URL_OFFICE).addConverterFactory(GsonConverterFactory.create()).build();

            HttpService service = retrofit.create(HttpService.class);
            Call<User> getUser = service.getUser(userid);

            User user = getUser.execute().body();

            tvFname.setText(user.fname + " " + user.lname);
            tvIDnumber.setText("User ID: "+ user.stud_id);
            tvCourse.setText(/*"Course: " +*/ user.coursecode);
            tvCourseDesc.setText(/*"Course Description: " + */user.description);
            Picasso.with(this).load(DynamicStrings.BASE_URL_OFFICE + "student/picture/" + user.picture).resize(200, 200).into(imgUserPhoto);

            //Log.d("IMAGE URL: ", DynamicStrings.BASE_URL_OFFICE + "student/picture/" + user.picture);

            //Log.e("User:", user.stud_id + " " + user.fname + " " + user.lname);

        } catch (Exception e) {
            Log.e("Login: ", "" + e);
        }
        ;
    }
}
