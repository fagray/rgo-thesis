package app.student.rgo5.com.rgo5.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import app.student.rgo5.com.rgo5.R;
import app.student.rgo5.com.rgo5.adapter.AnnouncementListViewAdapter;
import app.student.rgo5.com.rgo5.dynamicstrings.DynamicStrings;
import app.student.rgo5.com.rgo5.httpservice.HttpService;
import app.student.rgo5.com.rgo5.objects.Announcement;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by User on 8/24/2016.
 */
public class AnnouncementActivity extends Activity {
    ListView lv;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.announcements);

        lv = (ListView) findViewById(R.id.listView_announcements);
        populate();

    }

    public void populate() {

        try {


            Retrofit retrofit = new Retrofit.Builder().baseUrl(DynamicStrings.BASE_URL_OFFICE).addConverterFactory(GsonConverterFactory.create()).build();

            HttpService service = retrofit.create(HttpService.class);
            Call<List<Announcement>> getAll = service.getAnnouncement();
            List<Announcement> announcement= getAll.execute().body();

            //Log.e("List: ", announcement.toArray().toString());
            if(announcement!=null) {
                AnnouncementListViewAdapter myadapter = new AnnouncementListViewAdapter(AnnouncementActivity.this,
                        R.layout.list_item, announcement);
                lv.setAdapter(myadapter);
            } else {
                ArrayList<String> items = new ArrayList<String>();
                items.add("No grades available");
                ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(AnnouncementActivity.this, android.R.layout.simple_list_item_1,items);
                lv.setAdapter(stringArrayAdapter);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
