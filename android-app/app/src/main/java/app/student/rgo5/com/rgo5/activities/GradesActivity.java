package app.student.rgo5.com.rgo5.activities;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import app.student.rgo5.com.rgo5.R;
import app.student.rgo5.com.rgo5.adapter.AnnouncementListViewAdapter;
import app.student.rgo5.com.rgo5.adapter.GradesListViewAdapter;
import app.student.rgo5.com.rgo5.dynamicstrings.DynamicStrings;
import app.student.rgo5.com.rgo5.httpservice.HttpService;
import app.student.rgo5.com.rgo5.objects.Announcement;
import app.student.rgo5.com.rgo5.objects.Grade;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by User on 8/24/2016.
 */
public class GradesActivity extends Activity {

    public static final String EXTRA_USER_ID = "user_id";

    ListView listView;
    String user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.grades);

        Bundle extras = getIntent().getExtras();
        user_id = extras.get(EXTRA_USER_ID).toString();
        listView = (ListView) findViewById(R.id.listView_grades);

        populate();

    }

    public void populate() {

        try {


            Retrofit retrofit = new Retrofit.Builder().baseUrl(DynamicStrings.BASE_URL_OFFICE).addConverterFactory(GsonConverterFactory.create()).build();

            HttpService service = retrofit.create(HttpService.class);
            Call<List<Grade>> getAll = service.getGrades(user_id);
            List<Grade> grades = getAll.execute().body();


            if (grades!=null) {
                GradesListViewAdapter gradesListViewAdapter = new GradesListViewAdapter(GradesActivity.this,
                        R.layout.grade_list_item, grades);
                listView.setAdapter(gradesListViewAdapter);
            } else {

                ArrayList<String> items = new ArrayList<String>();
                items.add("No grades available");
                ArrayAdapter<String> stringArrayAdapter = new ArrayAdapter<String>(GradesActivity.this, android.R.layout.simple_list_item_1,items);
                listView.setAdapter(stringArrayAdapter);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
