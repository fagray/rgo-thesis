package app.student.rgo5.com.rgo5.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import app.student.rgo5.com.rgo5.R;
import app.student.rgo5.com.rgo5.dynamicstrings.DynamicStrings;
import app.student.rgo5.com.rgo5.objects.Announcement;

/**
 * Created by User on 8/26/2016.
 */
public class AnnouncementListViewAdapter extends ArrayAdapter<Announcement>{

    Context context;

    public AnnouncementListViewAdapter(Context context, int resourceId, List<Announcement> items){
        super(context, R.layout.list_item, items);
        this.context = context;



    }

    class ViewHolder {
        ImageView imageView;
        TextView title;
        TextView date;
        TextView desc;
    }

    public View getView(int position, View convertView, ViewGroup parent){
        ViewHolder holder = null;

        Announcement announcement = getItem(position);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.list_item, null);
            //convertView = mInflater.inflate(R.layout.list_item, parent);
            holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.tv_list_title);
            holder.date = (TextView) convertView.findViewById(R.id.tv_list_date);
            holder.desc = (TextView) convertView.findViewById(R.id.tv_list_content);
            holder.imageView = (ImageView) convertView.findViewById(R.id.image_announcement);

            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();
        holder.title.setText(announcement.title);
        holder.date.setText(announcement.date_create + " " + announcement.time_create);

        holder.desc.setText(announcement.body.toString());

        Picasso.with(context).load(DynamicStrings.BASE_URL_OFFICE + "announcement/picture/" + announcement.picture).resize(200, 200).into(holder.imageView);

        return convertView;

    }

}