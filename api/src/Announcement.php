<?php 

require_once('../config/config.php');

class Announcement{

	private $db;
	
	public function __construct(){
		$this->db = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
		if(mysqli_connect_errno()){
			echo 'Error: Could not connect to Database';
			exit;
		}

	}
	
	public function all()
	{
	
		$type = array();
		$sql = "SELECT * FROM announcements_tbl";		
		$check=$this->db->query($sql)or 
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){		
		$result = mysqli_query($this->db, $sql);
		while($row=mysqli_fetch_assoc($result)){
			
			$type[]= array('anno_id' => $row['anno_id'], 'title' => $row['title'],'details' => $row['details'],'priority' => $row['priority'],'date' => $row['date']);
			
		}
		return $type;
		}else
		return null;
	}


}



 ?>