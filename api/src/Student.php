<?php
date_default_timezone_set("Asia/Taipei");
require_once('../config/config.php');

class Student{

	private $db;

	public function __construct(){
		$this->db = new mysqli(DB_SERVER,DB_USERNAME,DB_PASSWORD,DB_DATABASE);
		if(mysqli_connect_errno()){
			echo 'Error: Could not connect to Database';
			exit;
		}

	}

	public function getGrades($student_number, $batch_id)
	{
		$type = array();
		$sql = "SELECT * FROM tbl_grade WHERE stud_id = '$student_number' AND batchid = $batch_id";
		$check=$this->db->query($sql)or
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){
			$result = mysqli_query($this->db, $sql);
			while($row=mysqli_fetch_assoc($result)){


				$type[]= array(

						'grade_id' 		=> 		$row['grade_id'],
						'grade' 		=> 		$row['grade'],
						'classid' 		=> 		$row['classid'],
						'stud_id' 		=> 		$row['stud_id'],
						'subjectid'		=>		$row['subjectid'],
						'date_create'	=>		$row['date_create'],
						'time_create' 	=> 		$row['time_create'],
						'date_update'	=>		$row['date_update'],
						'time_update'	=>		$row['time_update']

					);


			}

			return $type;
		}

		return null;
	}

	public function getSchedules($batch_id)
	{
		$type = array();

		//$today = strtotime('now');
		$datenow = date("Y-m-d");
		$sql = "SELECT * FROM tbl_schedclass WHERE batchid = '$batch_id' AND date >= '$datenow' ORDER BY date ASC LIMIT 1";
		$check=$this->db->query($sql)or
			die(mysqli_connect_errno()."nd okay.");
		$count_row=$check->num_rows;
		if($count_row>0){
			$result = mysqli_query($this->db, $sql);
			while($row=mysqli_fetch_assoc($result)){


				$schedules = array(

						'schedid' 		=> 		$row['schedid'],
						'move' 			=> 		$row['move'],
						'classid' 		=> 		$row['classid'],
						'tidnum' 		=> 		$row['tidnum'],
						'subject' 		=> 		$row['subject'],
						'batchid'		=>		$row['batchid'],
						'date'			=>		$row['date'],
						'fromtime'		=>		$row['fromtime'],
						'totime'		=>		$row['totime'],
						'note'			=>		$row['note'],
						'date_create'	=>		$row['date_create'],
						'time_create' 	=> 		$row['time_create'],
						'date_update'	=>		$row['date_update'],
						'time_update'	=>		$row['time_update']

					);


			}

			return $schedules;
		}

		return null;
	}

	public function getProfile($student_number, $batch_id)
	{
		$sql = "SELECT * FROM tbl_students as s,tbl_course as c WHERE s.stud_id = '$student_number' AND s.batchid = '$batch_id' AND s.course = c.coursecode  ";
		$result= mysqli_query($this->db, $sql);
		$profile = mysqli_fetch_array($result);
		return $profile;
	}


	public function login($stud_id,$password)
	{
		$password = md5($password);
		$sql = "SELECT * FROM tbl_students WHERE stud_id = '$stud_id' AND usr_password = '$password' ";
		$result= mysqli_query($this->db, $sql);
		// $grades = mysqli_fetch_array($result);
		if (  $result ){

				return mysqli_fetch_array($result);

			//return print $rows;


		}
		 return false;
	}


}



 ?>
