<link rel="stylesheet" type="text/css" href="../admin/css/table-style.css" />
<link rel="stylesheet" type="text/css" href="../admin/css/basictable.css" />
<script type="text/javascript" src="../admin/js/jquery.basictable.min.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
      $('#table').basictable();

      $('#table-breakpoint').basictable({
        breakpoint: 768
      });

      $('#table-swap-axis').basictable({
        swapAxis: true
      });

      $('#table-force-off').basictable({
        forceResponsive: false
      });

      $('#table-no-resize').basictable({
        noResize: true
      });

      $('#table-two-axis').basictable();

      $('#table-max-height').basictable({
        tableWrapper: true
      });
    });
</script>
<div class="banner">
    <h2>
        <a href="index.php">Home</a>
        <i class="fa fa-angle-right"></i>
        <span>Exams</span>
        <i class="fa fa-angle-right"></i>
        <a href="index.php?mod=exams&act=add">Add Exam</a>
    </h2>
</div>

<div class="agile-tables">
					<div class="w3l-table-info">
					  <h3>Exams</h3>
					    <table id="table">
						<thead>
						  <tr>
							<th>Exam Code</th>
							<th>Description</th>
							<th>Option</th>
						  </tr>
						</thead>
						<tbody>
<?php
    $exam = new Exam();
    $access = $exam->get_exam();
    foreach($access as $value){
    ?>
        <tr>
            <td><?php echo $id = $value['examcode'];?></td>            
            <td><?php echo $value['description'];?></td>
            <td><a href="#">Edit</a>|<a href="../library/process.exam.php?action=delete&examid=<?php echo $id?>">Delete</a></td>                
        </tr>
    <?php
    }
?>
						</tbody>
					  </table>
					</div>
</div>

